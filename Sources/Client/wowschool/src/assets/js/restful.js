import axios from 'axios';
import url from 'url'
export const RestFul =  {
    baseURL: 'http://localhost:8000',

    async get(link, data, params){
        link = this.baseURL + link;
        if(params != null){
            params = new url.URLSearchParams(params);
            link += "?" + params;
        }
        let res = await axios.get(link)
        .catch(e => {
            console.log(e);
            return null;
        });
        if(res != null){
            return res.data;
        }else return null;
    },

    async post(link, data){
        link = this.baseURL + link;
        let res = await axios.post(link,data)
        .catch(e => {
            console.log(e);
            return null;
        });
        if(res != null){
            return res.data;
        }else return null;
    },

    async del(link, params){
       link = this.baseURL + link;
       let res = await axios.delete(link)
       .catch(e => {
            console.log(e);
            return null;
        });
        if(res != null){
            return res.data;
        }else return null;
    },

    async put(link, data){
        link = this.baseURL + link;
        let res = await axios.put(link, data)
        .catch(e => {
            console.log(e);
            return null;
        });
        if(res != null){
            return res.data;
        }else return null;
    }
}