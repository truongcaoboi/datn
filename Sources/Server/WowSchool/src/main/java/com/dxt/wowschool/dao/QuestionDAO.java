package com.dxt.wowschool.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.dxt.wowschool.util.Util;
import com.dxt.wowschool.vo.Question;

@Component
public class QuestionDAO {
	public Question getQuestion(int id) {
		Question q = null;
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			conn = MysqlConnector.getMysqlConnector();
			String sql = "select q.*, s.subject_name, sp.subject_part_name"
					+ " from question as q, subjects as s,subject_part as sp"
					+ " where  "
					+ " q.question_id = ? and q.subject_id = s.subject_id and q.subject_part_id = sp.subject_part_id";
			state = conn.prepareStatement(sql);
			state.setInt(1, id);
			rs = state.executeQuery();
			while (rs.next()) {
				 q = new Question();
				q.setId(rs.getInt("question_id"));
				q.setCode(rs.getString("question_code"));
				q.setContent(rs.getString("question_content"));
				q.setAnswers(rs.getString("question_answers"));
				q.setImage(rs.getString("question_image"));
				q.setSubjectId(rs.getInt("subject_id"));
				q.setSubjectPartId(rs.getInt("subject_part_id"));
				q.setLevel(rs.getInt("question_level"));
				q.setSubjectName(rs.getString("subject_name"));
				q.setSubjectPartName(rs.getString("subject_part_name"));
				if(q.getLevel() == 1) {
					q.setLevelText("Dễ");
				} else if(q.getLevel() == 2) {
					q.setLevelText("Trung bình");
				} else if(q.getLevel() == 3) {
					q.setLevelText("Khó");
				} else if(q.getLevel() == 4) {
					q.setLevelText("Rất khó");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return q;
	}
	
	public List<Question> getAllQuestion() {
		List<Question> data = new ArrayList<Question>();
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			conn = MysqlConnector.getMysqlConnector();
			String sql = "select q.*, s.subject_name, sp.subject_part_name"
					+ " from question as q, subjects as s,subject_part as sp"
					+ " where  "
					+ " q.subject_id = s.subject_id and q.subject_part_id = sp.subject_part_id";
			state = conn.prepareStatement(sql);
			rs = state.executeQuery();
			while (rs.next()) {
				Question q = new Question();
				q.setId(rs.getInt("question_id"));
				q.setCode(rs.getString("question_code"));
				q.setContent(rs.getString("question_content"));
				q.setAnswers(rs.getString("question_answers"));
				q.setImage(rs.getString("question_image"));
				q.setSubjectId(rs.getInt("subject_id"));
				q.setSubjectPartId(rs.getInt("subject_part_id"));
				q.setLevel(rs.getInt("question_level"));
				q.setSubjectName(rs.getString("subject_name"));
				q.setSubjectPartName(rs.getString("subject_part_name"));
				if(q.getLevel() == 1) {
					q.setLevelText("Dễ");
				} else if(q.getLevel() == 2) {
					q.setLevelText("Trung bình");
				} else if(q.getLevel() == 3) {
					q.setLevelText("Khó");
				} else if(q.getLevel() == 4) {
					q.setLevelText("Rất khó");
				}
				data.add(q);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return data;
	}

	public List<Question> getAllQuestionFilter(int subjectId) {
		List<Question> data = new ArrayList<Question>();
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		if (subjectId == -1) {
			return getAllQuestion();
		}
		try {
			conn = MysqlConnector.getMysqlConnector();
			String sql = "select q.*, s.subject_name, sp.subject_part_name"
					+ " from question as q, subjects as s,subject_part as sp"
					+ " where q.subject_id = ? "
					+ "and q.subject_id = s.subject_id and q.subject_part_id = sp.subject_part_id";
			state = conn.prepareStatement(sql);
			state.setInt(1, subjectId);
			rs = state.executeQuery();
			while (rs.next()) {
				Question q = new Question();
				q.setId(rs.getInt("question_id"));
				q.setCode(rs.getString("question_code"));
				q.setContent(rs.getString("question_content"));
				q.setAnswers(rs.getString("question_answers"));
				q.setImage(rs.getString("question_image"));
				q.setSubjectId(rs.getInt("subject_id"));
				q.setSubjectPartId(rs.getInt("subject_part_id"));
				q.setLevel(rs.getInt("question_level"));
				q.setSubjectName(rs.getString("subject_name"));
				q.setSubjectPartName(rs.getString("subject_part_name"));
				if(q.getLevel() == 1) {
					q.setLevelText("Dễ");
				} else if(q.getLevel() == 2) {
					q.setLevelText("Trung bình");
				} else if(q.getLevel() == 3) {
					q.setLevelText("Khó");
				} else if(q.getLevel() == 4) {
					q.setLevelText("Rất khó");
				}
				data.add(q);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return data;
	}

	public List<Question> getAllQuestionFilter(int subjectId, int subjectPartId) {
		List<Question> data = new ArrayList<Question>();
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		if (subjectPartId == -1) {
			return getAllQuestionFilter(subjectId);
		}
		try {
			conn = MysqlConnector.getMysqlConnector();
			String sql = "select q.*, s.subject_name, sp.subject_part_name"
					+ " from question as q, subjects as s,subject_part as sp"
					+ " where q.subject_id = ? and q.subject_part_id = ? "
					+ "and q.subject_id = s.subject_id and q.subject_part_id = sp.subject_part_id";
			state = conn.prepareStatement(sql);
			state.setInt(1, subjectId);
			state.setInt(2, subjectPartId);
			rs = state.executeQuery();
			while (rs.next()) {
				Question q = new Question();
				q.setId(rs.getInt("question_id"));
				q.setCode(rs.getString("question_code"));
				q.setContent(rs.getString("question_content"));
				q.setAnswers(rs.getString("question_answers"));
				q.setImage(rs.getString("question_image"));
				q.setSubjectId(rs.getInt("subject_id"));
				q.setSubjectPartId(rs.getInt("subject_part_id"));
				q.setLevel(rs.getInt("question_level"));
				q.setSubjectName(rs.getString("subject_name"));
				q.setSubjectPartName(rs.getString("subject_part_name"));
				if(q.getLevel() == 1) {
					q.setLevelText("Dễ");
				} else if(q.getLevel() == 2) {
					q.setLevelText("Trung bình");
				} else if(q.getLevel() == 3) {
					q.setLevelText("Khó");
				} else if(q.getLevel() == 4) {
					q.setLevelText("Rất khó");
				}
				data.add(q);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return data;
	}

	public List<Question> getAllQuestionFilter(int subjectId, int subjectPartId, int levelId) {
		List<Question> data = new ArrayList<Question>();
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		if (levelId == -1) {
			return getAllQuestionFilter(subjectId, subjectPartId);
		}
		try {
			conn = MysqlConnector.getMysqlConnector();
			String sql = "";
			if(subjectId == -1) {
				sql = "select q.*, s.subject_name, sp.subject_part_name"
						+ " from question as q, subjects as s,subject_part as sp"
						+ " where q.question_level = ? "
						+ "and q.subject_id = s.subject_id and q.subject_part_id = sp.subject_part_id";
				state = conn.prepareStatement(sql);
				state.setInt(1, levelId);
			}else if(subjectPartId == -1) {
				sql = "select q.*, s.subject_name, sp.subject_part_name"
						+ " from question as q, subjects as s,subject_part as sp"
						+ " where q.subject_id = ?  and q.question_level = ? "
						+ "and q.subject_id = s.subject_id and q.subject_part_id = sp.subject_part_id";
				state = conn.prepareStatement(sql);
				state.setInt(1, subjectId);
				state.setInt(2, levelId);
			}else {
				sql = "select q.*, s.subject_name, sp.subject_part_name"
						+ " from question as q, subjects as s,subject_part as sp"
						+ " where q.subject_id = ? and q.subject_part_id = ? and q.question_level = ? "
						+ "and q.subject_id = s.subject_id and q.subject_part_id = sp.subject_part_id";
				state = conn.prepareStatement(sql);
				state.setInt(1, subjectId);
				state.setInt(2, subjectPartId);
				state.setInt(3, levelId);
			}
			rs = state.executeQuery();
			while (rs.next()) {
				Question q = new Question();
				q.setId(rs.getInt("question_id"));
				q.setCode(rs.getString("question_code"));
				q.setContent(rs.getString("question_content"));
				q.setAnswers(rs.getString("question_answers"));
				q.setImage(rs.getString("question_image"));
				q.setSubjectId(rs.getInt("subject_id"));
				q.setSubjectPartId(rs.getInt("subject_part_id"));
				q.setLevel(rs.getInt("question_level"));
				q.setSubjectName(rs.getString("subject_name"));
				q.setSubjectPartName(rs.getString("subject_part_name"));
				if(q.getLevel() == 1) {
					q.setLevelText("Dễ");
				} else if(q.getLevel() == 2) {
					q.setLevelText("Trung bình");
				} else if(q.getLevel() == 3) {
					q.setLevelText("Khó");
				} else if(q.getLevel() == 4) {
					q.setLevelText("Rất khó");
				}
				data.add(q);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return data;
	}

	public int addQuestion(Question q) {
		int res = -1;
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			conn = MysqlConnector.getMysqlConnector();
			String sql = "insert into question(question_code, question_content, question_image, question_answers, subject_part_id, subject_id, question_level)"
					+ " values(?,?,?,?,?,?,?)";
			state = conn.prepareStatement(sql);
			state.setString(1, q.getCode());
			state.setString(2, q.getContent());
			state.setString(3, q.getImage());
			state.setString(4, q.getAnswers());
			state.setInt(5, q.getSubjectPartId());
			state.setInt(6, q.getSubjectId());
			state.setInt(7, q.getLevel());
			res = state.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return res;
	}

	public int update(Question q) {
		int res = -1;
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			conn = MysqlConnector.getMysqlConnector();
			String sql = "update question set question_code = ? , question_content = ?, question_image = ?,"
					+ "question_answers = ?, subject_part_id = ?, subject_id = ?, question_level = ? where question_id = ?";
			state = conn.prepareStatement(sql);
			state.setString(1, q.getCode());
			state.setString(2, q.getContent());
			state.setString(3, q.getImage());
			state.setString(4, q.getAnswers());
			state.setInt(5, q.getSubjectPartId());
			state.setInt(6, q.getSubjectId());
			state.setInt(7, q.getLevel());
			state.setInt(8, q.getId());
			res = state.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return res;
	}

	public String getNewCode() {
		String code = "";
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			conn = MysqlConnector.getMysqlConnector();
			String sql = "select max(question_id) as id from question";
			state = conn.prepareStatement(sql);
			rs = state.executeQuery();
			int idmax = 0;
			while(rs.next()) {
				idmax = rs.getInt("id");
			}
			idmax++;
			code = "QUE"+Util.addPrefix(idmax+"", 4, '0');
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return code;
	}
}
