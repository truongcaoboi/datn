alter table class add constraint fk_class_teacher foreign key (class_teacher) references teacher(teacher_id);
alter table class_exam add constraint fk_class_exam_class foreign key (class_id) references class(class_id);
alter table class_exam add constraint fk_class_exam_exam foreign key (exam_id) references exam(exam_id);
alter table exam add constraint fk_exam_subject foreign key (exam_subject) references subjects(subject_id);
alter table exam add constraint fk_exam_teacher foreign key (teacher_id) references teacher(teacher_id);
alter table exam_question add constraint fk_exam_question_exam foreign key (exam_id) references exam(exam_id);
alter table exam_question add constraint fk_exam_question_question foreign key (question_id) references question(question_id);
alter table question add constraint fk_question_subject_part foreign key (subject_part_id) references subject_part(subject_part_id);
alter table student_exam add constraint fk_student_exam_student foreign key (student_id) references student(student_id);
alter table student_exam add constraint fk_student_exam_exam foreign key (exam_id) references exam(exam_id);
alter table student_semester add constraint fk_student_semester_student foreign key (student_id) references student(student_id);
alter table student_semester add constraint fk_student_semester_class foreign key (class_id) references class(class_id);
alter table student_semester add constraint fk_student_semester_sem foreign key (sem_id) references semester(sem_id);
alter table subject_part add constraint fk_subject_part_subject foreign key (subject_id) references subjects(subject_id);
alter table teacher_subject add constraint fk_teacher_subject_teacher foreign key (teacher_id) references teacher(teacher_id);
alter table teacher_subject add constraint fk_teacher_subject_subject foreign key (subject_id) references subjects(subject_id);