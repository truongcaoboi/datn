package com.dxt.wowschool.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dxt.wowschool.dao.ClassDAO;
import com.dxt.wowschool.dao.SemesterDAO;
import com.dxt.wowschool.vo.ClassRoom;
import com.dxt.wowschool.vo.Semester;

@RestController
@RequestMapping("/api/semesters")
public class SemesterController {
	@Autowired
	private SemesterDAO dao;
	@Autowired
	private ClassDAO cdao;
	@GetMapping("/allSemester")
	public List<Semester> getAllSemester(){
		return dao.getAllSemester();
	}
	
	@GetMapping("/newCode")
	public String getNewCodeSemester() {
		return dao.getNextCode();
	}
	
	@GetMapping("/getbyid/{id}")
	public Semester getSemesterById(@PathVariable int id) {
		return dao.getSemesterById(id);
	}
	
	@PostMapping("/addSemester")
	public Map<String, Object> addSemester(@RequestBody Semester sem){
		Map<String,Object> map = new HashMap<>();
		map.put("status", dao.addSemester(sem));
		return map;
	}
	
	@PutMapping("/updateSemester")
	public Map<String, Object> updateSemester(@RequestBody Semester sem){
		Map<String,Object> map = new HashMap<>();
		map.put("status", dao.updateSemester(sem));
		return map;
	}
	
	@DeleteMapping("/deleteSemester/{id}")
	public Map<String, Object> deleteSemester(@PathVariable int id){
		Map<String,Object> map = new HashMap<>();
		map.put("status", dao.deleteSemester(id));
		return map;
	}
	
	@PostMapping("/addClassSemester")
	public Map<String, Object> deleteSemester(@RequestBody Map<String,Object> body){
		Map<String,Object> map = new HashMap<>();
		map.put("status", 0);
		int classId = (int) body.get("classId");
		int semId = (int) body.get("semId");
		ClassRoom cl = cdao.getClassRoom(classId);
		if(cl.getSs() > 0) {
			map = dao.addClass(cl, semId);
		}else {
			map.put("mes", "Lớp không có học sinh!");
		}
		return map;
	}
	
	@PutMapping("/tongket/{id}")
	public Map<String, Object> tongket(@PathVariable int id){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("status", dao.tongket(id));
		return map;
	}
}
