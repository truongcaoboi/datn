-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: wowschooltest
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `student_exam`
--

DROP TABLE IF EXISTS `student_exam`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student_exam` (
  `student_exam_id` int NOT NULL AUTO_INCREMENT,
  `student_id` int DEFAULT NULL,
  `exam_id` int DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `exam_random` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `exam_result` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `time_finish` timestamp NULL DEFAULT NULL,
  `result_temp` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `mark` double DEFAULT NULL,
  `status` int DEFAULT NULL,
  PRIMARY KEY (`student_exam_id`),
  KEY `fk_student_exam_student` (`student_id`),
  KEY `fk_student_exam_exam` (`exam_id`),
  CONSTRAINT `fk_student_exam_exam` FOREIGN KEY (`exam_id`) REFERENCES `exam` (`exam_id`),
  CONSTRAINT `fk_student_exam_student` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_exam`
--

LOCK TABLES `student_exam` WRITE;
/*!40000 ALTER TABLE `student_exam` DISABLE KEYS */;
INSERT INTO `student_exam` VALUES (15,5,2,'2021-06-19 15:42:36','[[2,3,0,1,2],[4,1,0,2,3],[5,2,3,0,1],[3,2,0,3,1],[1,3,2,0,1]]','[1,1,2,1,2]','2021-06-19 15:54:44','[1,2,0,3,2]',4,0),(16,5,2,'2021-06-19 15:45:44','[[4,2,0,3,1],[3,2,0,1,3],[5,1,2,3,0],[1,2,1,0,3],[2,1,3,2,0]]','[1,1,3,2,3]','2021-06-19 15:56:56','[0,2,1,2,0]',2,0),(17,5,2,'2021-06-19 15:47:43','[[5,1,2,0,3],[3,0,1,2,3],[1,0,3,2,1],[2,0,1,2,3],[4,0,3,1,2]]','[2,0,0,0,0]','2021-06-19 15:58:47','[2,0,0,0,0]',10,0),(18,5,2,'2021-06-19 15:52:27','[[3,3,1,2,0],[5,3,1,2,0],[4,1,0,3,2],[1,0,1,2,3],[2,2,1,3,0]]','[3,3,1,0,3]','2021-06-19 16:02:29','[-1,-1,-1,-1,-1]',0,0),(19,5,2,'2021-06-19 16:03:53','[[2,3,0,1,2],[5,0,1,3,2],[3,1,2,3,0],[4,2,0,3,1],[1,0,1,3,2]]','[1,0,3,1,0]','2021-06-19 16:13:56','[-1,-1,-1,-1,-1]',0,0),(20,5,2,'2021-06-19 16:17:55','[[3,2,3,0,1],[2,3,1,2,0],[1,2,3,0,1],[4,2,3,1,0],[5,0,3,2,1]]','[2,3,2,3,0]','2021-06-19 16:44:17','[-1,-1,-1,-1,-1]',0,0),(21,5,3,'2021-06-20 01:57:15','[[2,3,1,0,2],[1,2,0,3,1],[3,0,1,3,2]]','[2,1,0]','2021-06-20 01:58:14','[0,1,2]',3.33,0),(22,5,3,'2021-06-20 01:58:44','[[2,2,1,0,3],[3,1,2,3,0],[1,3,0,1,2]]','[2,3,1]','2021-06-20 02:04:16','[0,1,2]',0,0),(23,5,3,'2021-06-20 02:12:29','[[2,1,3,0,2],[3,2,1,0,3],[1,3,1,2,0]]','[2,2,3]','2021-06-20 02:12:30','[-1,-1,-1]',0,0),(24,5,3,'2021-06-20 02:13:06','[[2,1,3,0,2],[1,0,3,2,1],[3,2,0,3,1]]','[2,0,1]','2021-06-20 02:13:08','[-1,-1,-1]',0,0),(25,5,3,'2021-06-20 02:15:09','[[2,0,3,1,2],[3,1,0,2,3],[1,2,3,0,1]]','[0,1,2]','2021-06-20 02:15:10','[-1,-1,-1]',0,0),(26,5,3,'2021-06-20 02:29:47','[[1,0,3,1,2],[2,3,2,0,1],[3,3,0,2,1]]','[0,2,1]','2021-06-20 02:34:49','[-1,-1,-1]',0,0),(27,5,2,'2021-06-28 08:46:50','[[3,0,3,2,1],[2,0,3,1,2],[1,1,3,0,2],[4,0,2,3,1],[5,3,0,1,2]]','[0,0,2,0,1]','2021-06-28 08:57:27','[-1,-1,-1,-1,-1]',0,0),(28,5,4,'2021-06-29 09:52:38','[[11,0,1,3,2],[10,1,3,0,2],[9,0,1,3,2],[12,1,0,3,2],[13,2,1,0,3]]','[1,1,1,3,0]','2021-06-29 09:57:27','[1,3,3,2,0]',4,0);
/*!40000 ALTER TABLE `student_exam` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-29 17:22:44
