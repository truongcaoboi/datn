-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: wowschooltest
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `administrator`
--

DROP TABLE IF EXISTS `administrator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `administrator` (
  `admin_id` int NOT NULL AUTO_INCREMENT,
  `admin_code` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `admin_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `admin_password` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `admin_avatar` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `admin_gender` int DEFAULT NULL,
  `admin_birth` timestamp NULL DEFAULT NULL,
  `admin_address` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `admin_identify` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `admin_identify_address` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `admin_identify_date` timestamp NULL DEFAULT NULL,
  `admin_phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `admin_email` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `admin_role` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administrator`
--

LOCK TABLES `administrator` WRITE;
/*!40000 ALTER TABLE `administrator` DISABLE KEYS */;
INSERT INTO `administrator` VALUES (1,'AD0001','Đặng Xuân Trường','1234','/resource/images/942e8b70-edb4-4829-9eb0-54c4d9ede0bdimages.jfif',1,'1997-01-18 17:00:00','Hà Nội','1111','Hà Nội','2021-04-30 17:00:00','0123456789','gmail@gmail.com',1,1),(10,'AD0002','Vũ Văn Thận',NULL,'/resource/images/0e8f127e-ae62-467b-8536-3f5d70232185Tổng-hợp-hình-nền-full-HD-1920-x-1080-đẹp-nhất-2.jpg',0,'1991-06-10 17:00:00','Hà Nội','0123456789','Hà Nội','2011-07-11 17:00:00','0123456789','thanvv@gmail.com',0,1),(12,'AD0011','Đỗ Ngọc Vũ',NULL,'',2,'1989-06-12 17:00:00','Hà Nội','902345609812','Hà Nội','2020-02-03 17:00:00','0354787653','vungocdo@gmail.com',0,1),(13,'AD0013','Nguyễn Hoàng Anh',NULL,'',2,'1987-06-16 17:00:00','Hà Nội','123098709123','Hà Nội','2020-02-03 17:00:00','0324589901','anhhoangnguyen@gmail.com',0,1),(14,'AD0014','Nguyễn Đức Sâm',NULL,'',1,'1979-05-15 17:00:00','Hà Nội','023489031289','Hà Nội','2020-06-09 17:00:00','0345891254','samnguyenduc@gmail.com',0,1),(15,'AD0015','Hoàng Như Mai',NULL,'',2,'2021-06-21 17:35:02','Hà Nội','023489012345','Hà Nội','2020-06-15 17:00:00','0344889221','mainguyennhu@gmail.com',0,1),(16,'AD0016','Đỗ Đức Anh',NULL,'',1,'1990-07-11 17:00:00','Hà Nội','2398087659','Hà Nội','2020-06-09 17:00:00','0345890123','anhdd@gmail.com',0,1),(17,'AD0017','Nguyễn Văn Bảy',NULL,'',1,'1990-06-11 17:00:00','Hà Nội','','','2021-06-29 09:37:45','','',0,1);
/*!40000 ALTER TABLE `administrator` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-29 17:22:43
