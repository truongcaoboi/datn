package com.dxt.wowschool.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.dxt.wowschool.util.Util;
import com.dxt.wowschool.vo.Admin;

@Component
public class AdminDAO {
	public List<Admin> getAdmins(){
		List<Admin> admins = new ArrayList<Admin>();
		Connection conn = null;
		Statement state = null;
		ResultSet rs = null;
		try {
			String sql = "select * from administrator where status = 1";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.createStatement();
			rs = state.executeQuery(sql);
			while(rs.next()) {
				Admin ad = new Admin();
				ad.setId(rs.getInt("admin_id"));
				ad.setCode(rs.getString("admin_code"));
				ad.setName(rs.getString("admin_name"));
				ad.setAvatar(rs.getString("admin_avatar"));
				ad.setGender(rs.getInt("admin_gender"));
				ad.setBirth(Util.changeTimestampSqlToDate(rs.getTimestamp("admin_birth")));
				ad.setAddress(rs.getString("admin_address"));
				ad.setIdentify(rs.getString("admin_identify"));
				ad.setIdentifyAddress(rs.getString("admin_identify_address"));
				ad.setIdentifyDate(Util.changeTimestampSqlToDate(rs.getTimestamp("admin_identify_date")));
				ad.setPhone(rs.getString("admin_phone"));
				ad.setEmail(rs.getString("admin_email"));
				ad.setStatus(rs.getInt("status"));
				ad.setRole(rs.getInt("admin_role"));
				admins.add(ad);
			}
		} catch (Exception e) {
			e.printStackTrace();
			admins = new ArrayList<>();
		} finally {
			MysqlConnector.close(rs, state, null, conn);
		}
		return admins;
	}
	
	public List<Admin> searchAdmin(String code, String name){
		List<Admin> admins = new ArrayList<Admin>();
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			String sql = "select * from administrator "
					+ "where UPPER(admin_code) like '*?*' or UPPER(admin_name) like '*?*'";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setString(1, code.toUpperCase());
			state.setString(2, name.toUpperCase());
			rs = state.executeQuery();
			while(rs.next()) {
				Admin ad = new Admin();
				ad.setId(rs.getInt("admin_id"));
				ad.setCode(rs.getString("admin_code"));
				ad.setName(rs.getString("admin_name"));
				ad.setAvatar(rs.getString("admin_avatar"));
				ad.setGender(rs.getInt("admin_gender"));
				ad.setBirth(Util.changeTimestampSqlToDate(rs.getTimestamp("admin_birth")));
				ad.setAddress(rs.getString("admin_address"));
				ad.setIdentify(rs.getString("admin_identify"));
				ad.setIdentifyAddress(rs.getString("admin_identify_address"));
				ad.setIdentifyDate(Util.changeTimestampSqlToDate(rs.getTimestamp("admin_identify_date")));
				ad.setPhone(rs.getString("admin_phone"));
				ad.setEmail(rs.getString("admin_email"));
				ad.setStatus(rs.getInt("status"));
				ad.setRole(rs.getInt("admin_role"));
				admins.add(ad);
			}
		} catch (Exception e) {
			e.printStackTrace();
			admins = new ArrayList<>();
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return admins;
	}
	
	public Admin getAdmin(int id) {
		Admin ad = null;
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			String sql = "select * from administrator "
					+ "where admin_id = ?";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setInt(1, id);
			rs = state.executeQuery();
			while(rs.next()) {
				ad = new Admin();
				ad.setId(rs.getInt("admin_id"));
				ad.setCode(rs.getString("admin_code"));
				ad.setName(rs.getString("admin_name"));
				ad.setAvatar(rs.getString("admin_avatar"));
				ad.setGender(rs.getInt("admin_gender"));
				ad.setBirth(Util.changeTimestampSqlToDate(rs.getTimestamp("admin_birth")));
				ad.setAddress(rs.getString("admin_address"));
				ad.setIdentify(rs.getString("admin_identify"));
				ad.setIdentifyAddress(rs.getString("admin_identify_address"));
				ad.setIdentifyDate(Util.changeTimestampSqlToDate(rs.getTimestamp("admin_identify_date")));
				ad.setPhone(rs.getString("admin_phone"));
				ad.setEmail(rs.getString("admin_email"));
				ad.setStatus(rs.getInt("status"));
				ad.setRole(rs.getInt("admin_role"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			ad = null;
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return ad;
	}

	public String getNewAdminCode() {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		String sql = "select max(admin_id) as admin_id from administrator;";
		String code = null;
		try {
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			rs = state.executeQuery();
			int idMax = 0;
			while(rs.next()) {
				idMax = rs.getInt("admin_id");
			}
			idMax ++;
			code = "AD"+Util.addPrefix(""+idMax, 4, '0');
		} catch (Exception e) {
			e.printStackTrace();
			code = null;
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return code;
	}
	
	public Admin addAdmin(Admin ad) {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		int id = -1;
		String code = getNewAdminCode();
		if(code == null) {
			return null;
		}
		try {
			String sql = "insert into "
					+ "administrator(admin_code, admin_name,"
					+ "admin_avatar, admin_gender, admin_birth, admin_address,"
					+ "admin_identify, admin_identify_address, admin_identify_date,"
					+ "admin_phone, admin_email, admin_role, status) "
					+ "values (?,?,?,?,?,?,?,?,?,?,?,?,?)";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setString(1, code);
			state.setString(2, ad.getName());
			state.setString(3, ad.getAvatar());
			state.setInt(4, ad.getGender());
			state.setTimestamp(5,new Timestamp(ad.getBirth().getTime()));
			state.setString(6, ad.getAddress());
			state.setString(7, ad.getIdentify());
			state.setString(8, ad.getIdentifyAddress());
			state.setTimestamp(9,new Timestamp(ad.getIdentifyDate().getTime()));
			state.setString(10, ad.getPhone());
			state.setString(11, ad.getEmail());
			state.setInt(12, ad.getRole());
			state.setInt(13, 1);
			id = state.executeUpdate();
			ad.setId(id);
			ad.setCode(code);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		if(id == -1) {
			return null;
		}else {
			return ad;
		}
	}
	
	public int updateAdmin(Admin ad) {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		int result = -1;
		try {
			String sql = "update administrator set "
					+ "admin_name = ?, admin_gender = ?,"
					+ "admin_birth = ?, admin_address = ?,"
					+ "admin_identify = ?, admin_identify_address = ?,"
					+ "admin_identify_date = ?, admin_phone = ?,"
					+ "admin_email = ?, admin_role = ? "
					+ "where admin_id = ?";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setString(1, ad.getName());
			state.setInt(2, ad.getGender());
			state.setTimestamp(3,new Timestamp(ad.getBirth().getTime()));
			state.setString(4, ad.getAddress());
			state.setString(5, ad.getIdentify());
			state.setString(6, ad.getIdentifyAddress());
			state.setTimestamp(7,new Timestamp(ad.getIdentifyDate().getTime()));
			state.setString(8, ad.getPhone());
			state.setString(9, ad.getEmail());
			state.setInt(10, ad.getRole());
			state.setInt(11, ad.getId());
			result = state.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			result = -1;
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return result;
	}
	
	public int deleteAdmin(int id) {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		int result = -1;
		try {
			String sql = "delete from administrator "
					+ "where admin_id = ? and admin_role = 0";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setInt(1, id);
			result = state.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			result = -1;
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return result;
	}
	
	public boolean changePassword(int idAdmin, String newPassword) {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		boolean result = false;
		
		String sql = "update administrator set admin_password = ? where admin_id = ?";
		try {
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setString(1, newPassword);
			state.setInt(2, idAdmin);
			result = state.execute();
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return result;
	}
	
	public int verifyAccount(String code,String identify) {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		int result = -1;
		
		String sql = "select admin_id from administrator "
				+ "where admin_code = ? and admin_identify = ?";
		try {
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setString(1, code);
			state.setString(2, identify);
			rs = state.executeQuery();
			while(rs.next()) {
				result = rs.getInt("admin_id");
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = -1;
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return result;
	}
	
	public boolean changeAvatar(int idAdmin, String  newAvatar) {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		boolean result = false;
		
		String sql = "update administrator set admin_avatar = ? where admin_id = ?";
		try {
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setString(1, newAvatar);
			state.setInt(2, idAdmin);
			result = state.execute();
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return result;
	}
}
