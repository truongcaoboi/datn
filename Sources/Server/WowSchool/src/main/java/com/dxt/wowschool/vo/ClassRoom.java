package com.dxt.wowschool.vo;

import com.dxt.wowschool.util.Util;

public class ClassRoom {
	private int id;
	private String code;
	private String name;
	private int status;
	private int grade;
	private int[][] timeTable = null;
	private int ss;
	private int teacherMainId;
	private String teacherMainName;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getGrade() {
		return grade;
	}
	public void setGrade(int grade) {
		this.grade = grade;
	}
	public String getTimeTable() {
		return Util.gson.toJson(this.timeTable);
	}
	public void setTimeTable(String timeTable) {
		if(timeTable != null) {
			if(!timeTable.equals("")) {
				this.timeTable = Util.gson.fromJson(timeTable, int[][].class);
			}
		}
		if(timeTable == null) {
			this.timeTable = new TimeTableStudent().getTableTable();
		}
	}
	public int getSs() {
		return ss;
	}
	public void setSs(int ss) {
		this.ss = ss;
	}
	public int getTeacherMainId() {
		return teacherMainId;
	}
	public void setTeacherMainId(int teacherMainId) {
		this.teacherMainId = teacherMainId;
	}
	public String getTeacherMainName() {
		return teacherMainName;
	}
	public void setTeacherMainName(String teacherMainName) {
		this.teacherMainName = teacherMainName;
	}
	
	public void createTimeTable() {
		if(timeTable == null) {
			this.timeTable = new TimeTableStudent().getTableTable();
		}
	}
}
