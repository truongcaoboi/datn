package com.dxt.wowschool.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Component;

import com.dxt.wowschool.InfoSystem;
import com.dxt.wowschool.packet.PacketStudentExam;
import com.dxt.wowschool.util.Util;
import com.dxt.wowschool.vo.Exam;
import com.dxt.wowschool.vo.HistoryExam;
import com.dxt.wowschool.vo.Question;

@Component
public class ExamDAO {
	private String textSplit = "##dxt##";
	public Exam getExamById(int examId) {
		Exam ex = null;
		Connection conn  = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			conn = MysqlConnector.getMysqlConnector();
			String sql = "select exam.*, teacher.teacher_code, teacher.teacher_name, subjects.subject_name"
					+ " from exam, teacher,subjects "
					+ "where exam.exam_id = ? and exam.teacher_id = teacher.teacher_id "
					+ "and exam.exam_subject = subjects.subject_id";
			state = conn.prepareStatement(sql);
			state.setInt(1, examId);
			rs = state.executeQuery();
			while(rs.next()) {
				ex = new Exam();
				ex.setId(rs.getInt("exam_id"));
				ex.setCode(rs.getString("exam_code"));
				ex.setName(rs.getString("exam_name"));
				ex.setTime(rs.getLong("exam_time"));
				ex.setGrade(rs.getInt("exam_grade"));
				ex.setSubject(rs.getInt("exam_subject"));
				ex.setTeacherPublish(rs.getInt("teacher_id"));
				ex.setTeacherCode(rs.getString("teacher_code"));
				ex.setTeacherName(rs.getString("teacher_name"));
				ex.setSubjectName(rs.getString("subject_name"));
			}
			state.close();
			sql = "select q.question_code from exam_question as eq, question as q"
					+ " where eq.exam_id = ? and eq.question_id = q.question_id";
			state = conn.prepareStatement(sql);
			state.setInt(1, ex.getId());
			rs.close();
			rs = state.executeQuery();
			String codes = "";
			while(rs.next()) {
				codes += rs.getString("question_code")+",";
			}
			if(codes.length()>0) {
				codes = codes.substring(0,codes.length() - 1);
			}
			ex.setQuestionCodes(codes);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return ex;
	}
	
	public List<Exam> getExamFilter(int grade, int type){
		List<Exam> data = new ArrayList<Exam>();
		Connection conn  = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			conn = MysqlConnector.getMysqlConnector();
			String sql = "select exam.*, teacher.teacher_code, teacher.teacher_name, subjects.subject_name"
					+ " from exam, teacher,subjects "
					+ "where exam.exam_grade = ? and exam.teacher_id = teacher.teacher_id and exam.exam_subject = subjects.subject_id";
			if(type == InfoSystem.CODE_STUDENT) {
				sql += " and (select count(question_id) from exam_question as exq where exam.exam_id = exq.exam_id) <> 0";
			}
			state = conn.prepareStatement(sql);
			state.setInt(1, grade);
			rs = state.executeQuery();
			while(rs.next()) {
				Exam ex = new Exam();
				ex.setId(rs.getInt("exam_id"));
				ex.setCode(rs.getString("exam_code"));
				ex.setName(rs.getString("exam_name"));
				ex.setTime(rs.getLong("exam_time"));
				ex.setGrade(rs.getInt("exam_grade"));
				ex.setSubject(rs.getInt("exam_subject"));
				ex.setTeacherPublish(rs.getInt("teacher_id"));
				ex.setTeacherCode(rs.getString("teacher_code"));
				ex.setTeacherName(rs.getString("teacher_name"));
				ex.setSubjectName(rs.getString("subject_name"));
				data.add(ex);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return data;
	}
	
	public List<Exam> getExamFilter(int subjectId, int grade,int type){
		List<Exam> data = new ArrayList<Exam>();
		Connection conn  = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		if(subjectId == -1) {
			return getExamFilter(grade,type);
		}
		try {
			conn = MysqlConnector.getMysqlConnector();
			String sql = "select exam.*, teacher.teacher_code, teacher.teacher_name, subjects.subject_name"
					+ " from exam, teacher,subjects "
					+ "where exam.exam_subject = ? and exam.exam_grade = ? "
					+ "and exam.teacher_id = teacher.teacher_id and exam.exam_subject = subjects.subject_id";
			if(type == InfoSystem.CODE_STUDENT) {
				sql += " and (select count(question_id) from exam_question as exq where exam.exam_id = exq.exam_id) <> 0";
			}
			state = conn.prepareStatement(sql);
			state.setInt(1, subjectId);
			state.setInt(2, grade);
			rs = state.executeQuery();
			while(rs.next()) {
				Exam ex = new Exam();
				ex.setId(rs.getInt("exam_id"));
				ex.setCode(rs.getString("exam_code"));
				ex.setName(rs.getString("exam_name"));
				ex.setTime(rs.getLong("exam_time"));
				ex.setGrade(rs.getInt("exam_grade"));
				ex.setSubject(rs.getInt("exam_subject"));
				ex.setTeacherPublish(rs.getInt("teacher_id"));
				ex.setTeacherCode(rs.getString("teacher_code"));
				ex.setTeacherName(rs.getString("teacher_name"));
				ex.setSubjectName(rs.getString("subject_name"));
				data.add(ex);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return data;
	}
	
	public int addExam(Exam exam) {
		int res = -1;
		Connection conn  = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			conn = MysqlConnector.getMysqlConnector();
			String sql = "insert into exam(exam_code, exam_name, exam_time, exam_grade, exam_subject, teacher_id)"
					+ " values(?,?,?,?,?,?)";
			state = conn.prepareStatement(sql);
			state.setString(1, exam.getCode());
			state.setString(2, exam.getName());
			state.setLong(3, exam.getTime());
			state.setInt(4, exam.getGrade());
			state.setInt(5, exam.getSubject());
			state.setInt(6, exam.getTeacherPublish());
			res = state.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return res;
	}
	
	public int updateExam(Exam exam, int teacherId) {
		int res = -1;
		Connection conn  = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			conn = MysqlConnector.getMysqlConnector();
			String sql = "update exam set exam_code = ?, exam_name = ?, exam_time = ?, exam_grade=?,exam_subject = ?,teacher_id = ? where exam_id = ?";
			state = conn.prepareStatement(sql);
			state.setString(1, exam.getCode());
			state.setString(2, exam.getName());
			state.setLong(3, exam.getTime());
			state.setInt(4, exam.getGrade());
			state.setInt(5, exam.getSubject());
			state.setInt(6, teacherId);
			state.setInt(7, exam.getId());
			state.executeUpdate();
			
			sql = "delete from exam_question where exam_id = ?";
			state.close();
			state = conn.prepareStatement(sql);
			state.setInt(1, exam.getId());
			state.executeUpdate();
			
			if(exam.getQuestionCodes()!=null) {
				if(!exam.getQuestionCodes().trim().equals("")) {
					String[] qcodes = exam.getQuestionCodes().split(",");
					List<Integer> idsQ = new ArrayList<Integer>();
					for(String code : qcodes) {
						sql = "select question_id from question where question_code = ?";
						state = conn.prepareStatement(sql);
						state.setString(1, code);
						rs = state.executeQuery();
						while(rs.next()) {
							idsQ.add(rs.getInt("question_id"));
						}
					}
					
					for(int id : idsQ) {
						sql = "insert into exam_question(exam_id,question_id) values(?,?)";
						state = conn.prepareStatement(sql);
						state.setInt(1, exam.getId());
						state.setInt(2, id);
						state.executeUpdate();
					}
				}
			}
			res = 1;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return res;
	}

	public String getNewExamCode() {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		String sql = "select max(exam_id) as id from exam;";
		String code = null;
		try {
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			rs = state.executeQuery();
			int idMax = 0;
			while(rs.next()) {
				idMax = rs.getInt("id");
			}
			idMax ++;
			code = "EXM"+Util.addPrefix(""+idMax, 4, '0');
		} catch (Exception e) {
			e.printStackTrace();
			code = null;
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return code;
	}
	
	public boolean checkInExam(int studentId) {
		boolean flag = false;
		Connection conn  = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			conn = MysqlConnector.getMysqlConnector();
			String sql = "select se.student_exam_id,se.start_time, e.exam_time from student_exam as se, exam as e where student_id = ? and status = 1 and se.exam_id = e.exam_id";
			state = conn.prepareStatement(sql);
			state.setInt(1, studentId);
			rs = state.executeQuery();
			int count = 0;
			Calendar now = Calendar.getInstance();
			Calendar dateCheck = Calendar.getInstance();
			while(rs.next()) {
				dateCheck.setTimeInMillis(Util.changeTimestampSqlToDate(rs.getTimestamp("start_time")).getTime());
				long min = rs.getLong("exam_time");
				if((now.getTimeInMillis() - dateCheck.getTimeInMillis())<min*60*1000) {
					count++;
				}else {
					endStudentExam(rs.getInt("student_exam_id"));
				}
			}
			if(count > 0) {
				flag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return flag;
	}
	
	public int createStudentExam(int studentId, int examId) {
		int res = -1;
		Connection conn  = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		QuestionDAO qdao = new QuestionDAO();
		try {
			conn = MysqlConnector.getMysqlConnector();
			String sql = "select question_id from exam_question where exam_id = ?";
			state = conn.prepareStatement(sql);
			state.setInt(1, examId);
			rs = state.executeQuery();
			List<Question> questions = new ArrayList<>();
			List<Integer> idqs = new ArrayList<Integer>();
			while(rs.next()) {
				idqs.add(rs.getInt("question_id"));
			}
			for(int idq : idqs) {
				Question q = qdao.getQuestion(idq);
				if(q!=null) {
					questions.add(q);
				}
			}
			int[][] examRandom = new int[questions.size()][5];
			Random rd = new Random();
			List<Question> qRandom = new ArrayList<Question>();
			int result [] = new int[questions.size()];
			while(questions.size()>0) {
				qRandom.add(questions.remove(rd.nextInt(questions.size())));
			}
			
			for(int i = 0;i<qRandom.size();i++) {
				Question q = qRandom.get(i);
				examRandom[i][0] = q.getId();
				String[] ans = q.getAnswers().split(textSplit);
				int indexCor = Integer.parseInt(ans[0]);
				String ansCorrect = ans[indexCor + 1];
				List<String> ss = new ArrayList<>();
				List<String> ss2 = new ArrayList<>();
				for(int j = 1;j<ans.length;j++) {
					ss.add(ans[j]);
				}
				ss2.addAll(ss);
				List<String> srandom = new ArrayList<String>();
				while(ss2.size() > 0) {
					srandom.add(ss2.remove(rd.nextInt(ss2.size())));
				}
				for(int j = 0;j<srandom.size();j++) {
					String a = srandom.get(j);
					if(a.equals(ansCorrect)) {
						result[i] = j;
					}
					for(int k = 0;k<ss.size();k++) {
						if(a.equals(ss.get(k))) {
							examRandom[i][j+1] = k;
						}
					}
				}
			}
			
			
			int resultTemp [] = new int[result.length];
			for(int i = 0;i<resultTemp.length;i++) {
				resultTemp[i] = -1;
			}
			Calendar ca = Calendar.getInstance();
			sql = "insert into student_exam(student_id,exam_id,start_time,exam_random,exam_result,result_temp,mark,status) "
					+ "values(?,?,?,?,?,?,?,?)";
			state.close();
			state = conn.prepareStatement(sql);
			state.setInt(1, studentId);
			state.setInt(2, examId);
			state.setTimestamp(3, new Timestamp(ca.getTimeInMillis()));
			state.setString(4, Util.gson.toJson(examRandom));
			state.setString(5, Util.gson.toJson(result));
			state.setString(6, Util.gson.toJson(resultTemp));
			state.setDouble(7,0);
			state.setInt(8, 1);
			res = state.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return res;
	}
	
	public PacketStudentExam getStudentExam(int studentId) {
		PacketStudentExam packet = null;
		Connection conn  = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		QuestionDAO qdao = new QuestionDAO();
		try {
			conn = MysqlConnector.getMysqlConnector();
			String sql = "select exam.exam_time,exam.exam_code,exam.exam_name, student_exam.* from student_exam, exam where student_id = ? and status = 1 and student_exam.exam_id = exam.exam_id";
			state = conn.prepareStatement(sql);
			state.setInt(1, studentId);
			rs = state.executeQuery();
			int[][] qRandom = null;
			int[] resultTemp = null;
			int id = -1;
			long time = 0;
			Calendar now = Calendar.getInstance();
			Calendar dateCheck = Calendar.getInstance();
			String ecode = "";
			String ename = "";
			long maxTime = 0;
			while(rs.next()) {
				qRandom = Util.gson.fromJson(rs.getString("exam_random"), int[][].class);
				resultTemp = Util.gson.fromJson(rs.getString("result_temp"), int[].class);
				ecode = rs.getString("exam_code");
				ename = rs.getString("exam_name");
				maxTime = rs.getLong("exam_time");
				if(qRandom != null & resultTemp != null) {
					id = rs.getInt("student_exam_id");
					dateCheck.setTimeInMillis(rs.getTimestamp("start_time").getTime());
					time = rs.getLong("exam_time")*60 - (now.getTimeInMillis() - dateCheck.getTimeInMillis())/1000;
					if(time < 0) {
						time += 10;
					}
					break;
				}
			}
			if(id > 0) {
				packet = new PacketStudentExam();
				List<Question> questions = new ArrayList<Question>();
				for(int i = 0;i<qRandom.length;i++) {
					int [] danhdau = qRandom[i];
					Question q = qdao.getQuestion(danhdau[0]);
					String[] ans = q.getAnswers().split(textSplit);
					String anew = ans[danhdau[1]+1];
					for(int j = 2;j<danhdau.length;j++) {
						anew += textSplit + ans[danhdau[j]+1];
					}
					q.setAnswers(anew);
					questions.add(q);
				}
				packet.setQuestions(questions);
				packet.setId(id);
				packet.setResults(resultTemp);
				packet.setTimeLeft(time);
				packet.setExamCode(ecode);
				packet.setExamName(ename);
				packet.setMaxTime(maxTime);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return packet;
	}
	
	
	public int updateStudentExam(PacketStudentExam packet) {
		int res = -1;
		Connection conn  = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			conn = MysqlConnector.getMysqlConnector();
			String sql = "select se.start_time, e.exam_time "
					+ "from student_exam as se, exam as e where se.exam_id = e.exam_id "
					+ "and se.student_exam_id = ?";
			state = conn.prepareStatement(sql);
			state.setInt(1, packet.getId());
			rs = state.executeQuery();
			long time = 0;
			Calendar now = Calendar.getInstance();
			Calendar dateCheck = Calendar.getInstance();
			while(rs.next()) {
				dateCheck.setTimeInMillis(rs.getTimestamp("start_time").getTime());
				time = rs.getLong("exam_time");
			}
			if(time - (now.getTimeInMillis() - dateCheck.getTimeInMillis())/60/1000 + 2 > 0) {
				sql = "update student_exam set result_temp = ? where student_exam_id = ?";
				state.close();
				state = conn.prepareStatement(sql);
				state.setString(1, Util.gson.toJson(packet.getResults()));
				state.setInt(2, packet.getId());
				res = state.executeUpdate();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return res;
	}
	
	public double endStudentExam(int id) {
		double mark = -1;
		Connection conn  = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			conn = MysqlConnector.getMysqlConnector();
			String sql = "select * from student_exam where student_exam_id = ?";
			state = conn.prepareStatement(sql);
			state.setInt(1, id);
			rs = state.executeQuery();
			int[] result = null;
			int[] reTemp = null;
			while(rs.next()) {
				result = Util.gson.fromJson(rs.getString("exam_result"), int[].class);
				reTemp = Util.gson.fromJson(rs.getString("result_temp"), int[].class);
			}
			mark = calMark(result, reTemp, id);
					
			sql = "update student_exam set mark=?, status = 0, time_finish = ? where student_exam_id = ?";
			state.close();
			state = conn.prepareStatement(sql);
			state.setDouble(1, mark);
			state.setTimestamp(2, new Timestamp(new Date().getTime()));
			state.setInt(3, id);
			state.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return mark;
	}
	
	public double calMark(int result[], int resultTemp[], int id) {
		double mark = -1;
		try {
			int count = 0;
			for(int i = 0;i<result.length;i++) {
				if(result[i] == resultTemp[i]) {
					count++;
				}
			}
			mark = Math.floor(10.0/result.length*count*100)*1.0/100;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mark;
	}

	public List<HistoryExam> getHistory(int studentId){
		List<HistoryExam> data = new ArrayList<HistoryExam>();
		Connection conn  = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			conn = MysqlConnector.getMysqlConnector();
			String sql = "select e.exam_code, e.exam_name, se.* "
					+ " from exam as e, student_exam as se "
					+ "where se.student_id = ? and e.exam_id = se.exam_id order by se.time_finish DESC";
			state = conn.prepareStatement(sql);
			state.setInt(1, studentId);
			rs = state.executeQuery();
			while(rs.next()) {
				HistoryExam h = new HistoryExam();
				h.setId(rs.getInt("student_exam_id"));
				h.setExamId(rs.getInt("exam_id"));
				h.setStudentId(rs.getInt("student_id"));
				h.setExamName(rs.getString("exam_name"));
				h.setExamCode(rs.getString("exam_code"));
				h.setMark(rs.getDouble("mark"));
				h.setStartDate(rs.getTimestamp("start_time").getTime());
				h.setEndDate(rs.getTimestamp("time_finish").getTime());
				data.add(h);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return data;
	}
}
