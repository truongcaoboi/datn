package com.dxt.wowschool.dao;
import java.sql.*;
public class MysqlConnector {
	private static final String URL = "jdbc:mysql://localhost:3306/wowschooltest";
	private static final String USERNAME = "root";
	private static final String PASSWORD = "123456789";
	public static Connection getMysqlConnector() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection conn = DriverManager.getConnection(URL, USERNAME, PASSWORD);
			return conn;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static void close(ResultSet rs, Statement st, PreparedStatement ps, Connection conn) {
		closeResultSet(rs);
		closeStatement(st);
		closePrepareStatement(ps);
		closeConnection(conn);
	}
	
	public static void closeResultSet(ResultSet rs) {
		try {
			if(rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void closePrepareStatement(PreparedStatement ps) {
		try {
			if(ps != null) {
				ps.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void closeStatement(Statement st) {
		try {
			if(st != null) {
				st.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void closeConnection(Connection conn) {
		try {
			if(conn != null) {
				conn.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
