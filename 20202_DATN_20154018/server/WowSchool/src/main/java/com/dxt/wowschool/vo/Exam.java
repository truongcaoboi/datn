package com.dxt.wowschool.vo;

public class Exam {
	private int id;
	private String code;
	private String name;
	private long time;
	private int type;
	private int grade;
	private int subject;
	private String subjectName;
	private int teacherPublish;
	private String teacherCode;
	private String teacherName;
	private String questionCodes;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getTime() {
		return time;
	}
	public void setTime(long time) {
		this.time = time;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getGrade() {
		return grade;
	}
	public void setGrade(int grade) {
		this.grade = grade;
	}
	public int getSubject() {
		return subject;
	}
	public void setSubject(int subject) {
		this.subject = subject;
	}
	public int getTeacherPublish() {
		return teacherPublish;
	}
	public void setTeacherPublish(int teacherPublish) {
		this.teacherPublish = teacherPublish;
	}
	public String getTeacherCode() {
		return teacherCode;
	}
	public void setTeacherCode(String teacherCode) {
		this.teacherCode = teacherCode;
	}
	public String getTeacherName() {
		return teacherName;
	}
	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}
	public String getQuestionCodes() {
		return questionCodes;
	}
	public void setQuestionCodes(String questionCodes) {
		this.questionCodes = questionCodes;
	}
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	
	
}
