package com.dxt.wowschool.vo;

import com.dxt.wowschool.util.Util;

public class Teacher extends Human{
	private int[][] teacherTimeTable;

	public String getTeacherTimeTable() {
		return Util.gson.toJson(this.teacherTimeTable);
	}
	
	public int[][] getOrigin(){
		return teacherTimeTable;
	}

	public void setTeacherTimeTable(String teacherTimeTable) {
		if(teacherTimeTable != null) {
			if(!teacherTimeTable.equals("")) {
				this.teacherTimeTable = Util.gson.fromJson(teacherTimeTable, int[][].class);
			}
		}
		if(teacherTimeTable == null) {
			this.teacherTimeTable = new TimeTableTeacher().getTableTable();
		}
	}
	
	
	public void createTimeTable() {
		if(teacherTimeTable == null) {
			this.teacherTimeTable = new TimeTableTeacher().getTableTable();
		}
	}
	
}
