package com.dxt.wowschool.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.dxt.wowschool.packet.PacketClassInfo;
import com.dxt.wowschool.util.Util;
import com.dxt.wowschool.vo.ClassRoom;
import com.dxt.wowschool.vo.Student;
import com.dxt.wowschool.vo.Teacher;

@Component
public class StudentDAO {
	public List<Student> getStudents(){
		List<Student> students = new ArrayList<Student>();
		Connection conn = null;
		Statement state = null;
		ResultSet rs = null;
		try {
			String sql = "select * from student where status = 1";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.createStatement();
			rs = state.executeQuery(sql);
			while(rs.next()) {
				Student st = new Student();
				st.setId(rs.getInt("student_id"));
				st.setCode(rs.getString("student_code"));
				st.setName(rs.getString("student_name"));
				st.setAvatar(rs.getString("student_avatar"));
				st.setGender(rs.getInt("student_gender"));
				st.setBirth(Util.changeTimestampSqlToDate(rs.getTimestamp("student_birth")));
				st.setAddress(rs.getString("student_address"));
				st.setIdentify(rs.getString("student_identify"));
				st.setIdentifyAddress(rs.getString("student_identify_address"));
				st.setIdentifyDate(Util.changeTimestampSqlToDate(rs.getTimestamp("student_identify_date")));
				st.setPhone(rs.getString("student_phone"));
				st.setEmail(rs.getString("student_email"));
				st.setStatus(rs.getInt("status"));
				st.setClassId(rs.getInt("class_id"));
				st.setFatherName(rs.getString("father_name"));
				st.setMotherName(rs.getString("mother_name"));
				st.setFatherAddress(rs.getString("father_address"));
				st.setMotherAddress(rs.getString("mother_address"));
				st.setFatherJob(rs.getString("father_job"));
				st.setMotherJob(rs.getString("mother_job"));
				st.setFatherPhone(rs.getString("father_phone"));
				st.setMotherPhone(rs.getString("mother_phone"));
				students.add(st);
			}
		} catch (Exception e) {
			e.printStackTrace();
			students = new ArrayList<>();
		} finally {
			MysqlConnector.close(rs, state, null, conn);
		}
		return students;
	}
	
	public List<Student> getStudentsByClassId(int classId){
		List<Student> students = new ArrayList<Student>();
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			String sql = "select * from student where status = 1 and class_id = ?";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setInt(1, classId);
			rs = state.executeQuery();
			while(rs.next()) {
				Student st = new Student();
				st.setId(rs.getInt("student_id"));
				st.setCode(rs.getString("student_code"));
				st.setName(rs.getString("student_name"));
				st.setAvatar(rs.getString("student_avatar"));
				st.setGender(rs.getInt("student_gender"));
				st.setBirth(Util.changeTimestampSqlToDate(rs.getTimestamp("student_birth")));
				st.setAddress(rs.getString("student_address"));
				st.setIdentify(rs.getString("student_identify"));
				st.setIdentifyAddress(rs.getString("student_identify_address"));
				st.setIdentifyDate(Util.changeTimestampSqlToDate(rs.getTimestamp("student_identify_date")));
				st.setPhone(rs.getString("student_phone"));
				st.setEmail(rs.getString("student_email"));
				st.setStatus(rs.getInt("status"));
				st.setClassId(rs.getInt("class_id"));
				st.setFatherName(rs.getString("father_name"));
				st.setMotherName(rs.getString("mother_name"));
				st.setFatherAddress(rs.getString("father_address"));
				st.setMotherAddress(rs.getString("mother_address"));
				st.setFatherJob(rs.getString("father_job"));
				st.setMotherJob(rs.getString("mother_job"));
				st.setFatherPhone(rs.getString("father_phone"));
				st.setMotherPhone(rs.getString("mother_phone"));
				students.add(st);
			}
		} catch (Exception e) {
			e.printStackTrace();
			students = new ArrayList<>();
		} finally {
			MysqlConnector.close(rs, state, null, conn);
		}
		return students;
	}
	
	public PacketClassInfo getClassMate(int id){
		PacketClassInfo packet = new PacketClassInfo();
		List<Student> students = new ArrayList<Student>();
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			String sql = "select * from student where status = 1 and class_id = (select c.class_id from student as c where c.student_id = ?)";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setInt(1, id);
			rs = state.executeQuery();
			while(rs.next()) {
				Student st = new Student();
				st.setId(rs.getInt("student_id"));
				st.setCode(rs.getString("student_code"));
				st.setName(rs.getString("student_name"));
				st.setAvatar(rs.getString("student_avatar"));
				st.setGender(rs.getInt("student_gender"));
				st.setBirth(Util.changeTimestampSqlToDate(rs.getTimestamp("student_birth")));
				st.setAddress(rs.getString("student_address"));
				st.setIdentify(rs.getString("student_identify"));
				st.setIdentifyAddress(rs.getString("student_identify_address"));
				st.setIdentifyDate(Util.changeTimestampSqlToDate(rs.getTimestamp("student_identify_date")));
				st.setPhone(rs.getString("student_phone"));
				st.setEmail(rs.getString("student_email"));
				st.setStatus(rs.getInt("status"));
				st.setClassId(rs.getInt("class_id"));
				st.setFatherName(rs.getString("father_name"));
				st.setMotherName(rs.getString("mother_name"));
				st.setFatherAddress(rs.getString("father_address"));
				st.setMotherAddress(rs.getString("mother_address"));
				st.setFatherJob(rs.getString("father_job"));
				st.setMotherJob(rs.getString("mother_job"));
				st.setFatherPhone(rs.getString("father_phone"));
				st.setMotherPhone(rs.getString("mother_phone"));
				students.add(st);
			}
			packet.setStudents(students);
			ClassRoom cls = new ClassDAO().getClassRoom(students.get(0).getClassId());
			Teacher teacher = new TeacherDAO().getTeacher(cls.getTeacherMainId());
			packet.setCls(cls);
			packet.setTeacher(teacher);
		} catch (Exception e) {
			e.printStackTrace();
			students = new ArrayList<>();
		} finally {
			MysqlConnector.close(rs, state, null, conn);
		}
		return packet;
	}
	
	
	public List<Student> searchStudent(String code, String name){
		List<Student> students = new ArrayList<Student>();
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			String sql = "select * from student "
					+ "where (UPPER(student_code) like '*?*' or UPPER(student_name) like '*?*') and status = 1 ";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setString(1, code.toUpperCase());
			state.setString(2, name.toUpperCase());
			rs = state.executeQuery();
			while(rs.next()) {
				Student st = new Student();
				st.setId(rs.getInt("student_id"));
				st.setCode(rs.getString("student_code"));
				st.setName(rs.getString("student_name"));
				st.setAvatar(rs.getString("student_avatar"));
				st.setGender(rs.getInt("student_gender"));
				st.setBirth(Util.changeTimestampSqlToDate(rs.getTimestamp("student_birth")));
				st.setAddress(rs.getString("student_address"));
				st.setIdentify(rs.getString("student_identify"));
				st.setIdentifyAddress(rs.getString("student_identify_address"));
				st.setIdentifyDate(Util.changeTimestampSqlToDate(rs.getTimestamp("student_identify_date")));
				st.setPhone(rs.getString("student_phone"));
				st.setEmail(rs.getString("student_email"));
				st.setStatus(rs.getInt("status"));
				st.setClassId(rs.getInt("class_id"));
				st.setFatherName(rs.getString("father_name"));
				st.setMotherName(rs.getString("mother_name"));
				st.setFatherAddress(rs.getString("father_address"));
				st.setMotherAddress(rs.getString("mother_address"));
				st.setFatherJob(rs.getString("father_job"));
				st.setMotherJob(rs.getString("mother_job"));
				st.setFatherPhone(rs.getString("father_phone"));
				st.setMotherPhone(rs.getString("mother_phone"));
				students.add(st);
			}
		} catch (Exception e) {
			e.printStackTrace();
			students = new ArrayList<>();
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return students;
	}
	
	public Student getStudent(int id) {
		Student st = null;
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			String sql = "select * from student "
					+ "where student_id = ?";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setInt(1, id);
			rs = state.executeQuery();
			while(rs.next()) {
				st = new Student();
				st.setId(rs.getInt("student_id"));
				st.setCode(rs.getString("student_code"));
				st.setName(rs.getString("student_name"));
				st.setAvatar(rs.getString("student_avatar"));
				st.setGender(rs.getInt("student_gender"));
				st.setBirth(Util.changeTimestampSqlToDate(rs.getTimestamp("student_birth")));
				st.setAddress(rs.getString("student_address"));
				st.setIdentify(rs.getString("student_identify"));
				st.setIdentifyAddress(rs.getString("student_identify_address"));
				st.setIdentifyDate(Util.changeTimestampSqlToDate(rs.getTimestamp("student_identify_date")));
				st.setPhone(rs.getString("student_phone"));
				st.setEmail(rs.getString("student_email"));
				st.setStatus(rs.getInt("status"));
				st.setClassId(rs.getInt("class_id"));
				st.setFatherName(rs.getString("father_name"));
				st.setMotherName(rs.getString("mother_name"));
				st.setFatherAddress(rs.getString("father_address"));
				st.setMotherAddress(rs.getString("mother_address"));
				st.setFatherJob(rs.getString("father_job"));
				st.setMotherJob(rs.getString("mother_job"));
				st.setFatherPhone(rs.getString("father_phone"));
				st.setMotherPhone(rs.getString("mother_phone"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			st = null;
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return st;
	}

	public String getNewStudentCode() {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		String sql = "select max(student_id) as student_id from student;";
		String code = null;
		try {
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			rs = state.executeQuery();
			int idMax = 0;
			while(rs.next()) {
				idMax = rs.getInt("student_id");
			}
			idMax ++;
			code = "ST"+Util.addPrefix(""+idMax, 4, '0');
		} catch (Exception e) {
			e.printStackTrace();
			code = null;
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return code;
	}
	
	public Student addStudent(Student st) {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		int id = -1;
		String code = getNewStudentCode();
		if(code == null) {
			return null;
		}
		try {
			String sql = "insert into "
					+ "student(student_code, student_name,"
					+ "student_avatar, student_gender, student_birth, student_address,"
					+ "student_identify, student_identify_address, student_identify_date,"
					+ "student_phone, student_email, status,"
					+ "father_name, mother_name, father_address, mother_address, father_job, mother_job, father_phone, mother_phone, class_id) "
					+ "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setString(1, code);
			state.setString(2, st.getName());
			state.setString(3, st.getAvatar());
			state.setInt(4, st.getGender());
			state.setTimestamp(5,new Timestamp(st.getBirth().getTime()));
			state.setString(6, st.getAddress());
			state.setString(7, st.getIdentify());
			state.setString(8, st.getIdentifyAddress());
			state.setTimestamp(9,new Timestamp(st.getIdentifyDate().getTime()));
			state.setString(10, st.getPhone());
			state.setString(11, st.getEmail());
			state.setInt(12, 1);
			state.setString(13, st.getFatherName());
			state.setString(14, st.getMotherName());
			state.setString(15, st.getFatherAddress());
			state.setString(16, st.getMotherAddress());
			state.setString(17, st.getFatherJob());
			state.setString(18, st.getMotherJob());
			state.setString(19, st.getFatherPhone());
			state.setString(20, st.getMotherPhone());
			state.setInt(21, st.getClassId());
			id = state.executeUpdate();
			st.setId(id);
			st.setCode(code);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		if(id == -1) {
			return null;
		}else {
			return st;
		}
	}
	
	public int updateStudent(Student st) {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		int result = -1;
		try {
			String sql = "update student set "
					+ "student_name = ?, student_gender = ?,"
					+ "student_birth = ?, student_address = ?,"
					+ "student_identify = ?, student_identify_address = ?,"
					+ "student_identify_date = ?, student_phone = ?,"
					+ "student_email = ?, father_name = ?, mother_name = ?, father_address = ?, mother_address = ?,"
					+ "father_job = ?, mother_job = ?, father_phone = ?, mother_phone = ? , class_id = ? "
					+ "where student_id = ?";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setString(1, st.getName());
			state.setInt(2, st.getGender());
			state.setTimestamp(3,new Timestamp(st.getBirth().getTime()));
			state.setString(4, st.getAddress());
			state.setString(5, st.getIdentify());
			state.setString(6, st.getIdentifyAddress());
			state.setTimestamp(7,new Timestamp(st.getIdentifyDate().getTime()));
			state.setString(8, st.getPhone());
			state.setString(9, st.getEmail());
			state.setString(10, st.getFatherName());
			state.setString(11, st.getMotherName());
			state.setString(12, st.getFatherAddress());
			state.setString(13, st.getMotherAddress());
			state.setString(14, st.getFatherJob());
			state.setString(15, st.getMotherJob());
			state.setString(16, st.getFatherPhone());
			state.setString(17, st.getMotherPhone());
			state.setInt(18, st.getClassId());
			state.setInt(19, st.getId());
			result = state.executeUpdate();
			
			//tim sem gan nhat
			sql = "select class_id from student_semester where student_id = ? and "
					+ "sem_id = (select max(sem_id) from semester)";
			state = conn.prepareStatement(sql);
			state.setInt(1, st.getId());
			rs = state.executeQuery();
			int classid = -1;
			while(rs.next()) {
				classid = rs.getInt("class_id");
			}
			if(classid != st.getClassId()) {
				sql = "update student_semester set class_id = ? where student_id = ? and "
						+ "sem_id = (select max(sem_id) from semester)";
				state = conn.prepareStatement(sql);
				state.setInt(1, st.getClassId());
				state.setInt(2, st.getId());
				state.executeUpdate();
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = -1;
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return result;
	}
	
	public int deleteStudent(int id) {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		int result = -1;
		try {
			conn = MysqlConnector.getMysqlConnector();
			String sql = "delete from student_exam where student_id = ?;";
			state = conn.prepareStatement(sql);
			state.setInt(1, id);
			state.execute();
			state.close();
			sql =  "delete from student_semester where student_id = ?;";
			state = conn.prepareStatement(sql);
			state.setInt(1, id);
			state.execute();
			state.close();
			sql =  "delete from student where student_id = ?";
			state = conn.prepareStatement(sql);
			state.setInt(1, id);
			result = state.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			result = -1;
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return result;
	}
	
	public boolean changePassword(int idStudent, String newPassword) {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		boolean result = false;
		
		String sql = "update student set student_password = ? where student_id = ?";
		try {
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setString(1, newPassword);
			state.setInt(2, idStudent);
			result = state.execute();
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return result;
	}
	
	public int verifyAccount(String code,String identify) {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		int result = -1;
		
		String sql = "select student_id from student "
				+ "where student_code = ? and student_identify = ?";
		try {
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setString(1, code);
			state.setString(2, identify);
			rs = state.executeQuery();
			while(rs.next()) {
				result = rs.getInt("student_id");
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = -1;
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return result;
	}
	
	public boolean changeAvatar(int idStudent, String  newAvatar) {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		boolean result = false;
		
		String sql = "update student set student_avatar = ? where student_id = ?";
		try {
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setString(1, newAvatar);
			state.setInt(2, idStudent);
			result = state.execute();
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return result;
	}
	
	public double[][] getTableMark(Student st, int semId){
		double [][] re = new double[][] {
			{-2}
		};
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			String sql = "select * from student_semester where student_id = ? and class_id = ? "
					+ " and sem_id = (select max(sem_id) from semester)";
			if(semId > 0) {
				sql = "select * from student_semester where student_id = ?  and sem_id = ?";
			}
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setInt(1, st.getId());
			state.setInt(2, st.getClassId());
			if(semId > 0) {
				state.setInt(2, semId);
			}
			rs = state.executeQuery();
			while(rs.next()) {
				re = Util.gson.fromJson(rs.getString("table_mark"), double[][].class);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return re;
	}
	
	public int updateTableMark(int id, String tablemark){
		int re = -1;
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			Student st = getStudent(id);
			String sql = "update student_semester set table_mark = ? where student_id = ? and class_id = ? "
					+ " and sem_id = (select max(sem_id) from semester)";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setString(1, tablemark);
			state.setInt(2, st.getId());
			state.setInt(3, st.getClassId());
			re = state.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return re;
	}
	
	public String getTimeTable(int id){
		Student st = getStudent(id);
		ClassRoom cl = new ClassDAO().getClassRoom(st.getClassId());
		return cl.getTimeTable();
	}
}
