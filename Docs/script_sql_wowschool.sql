
create database wowschool;
use wowschool;
create table student(
	student_id int not null primary key auto_increment,
    student_code nvarchar(20),
    student_name nvarchar(200),
    student_password nvarchar(500),
    student_avatar nvarchar(500),
    student_gender int,
    student_birth timestamp,
    student_address nvarchar(500),
    student_identify nvarchar(20),
    student_identify_address nvarchar(500),
    student_identify_date timestamp,
    student_phone nvarchar(20),
    student_email nvarchar(200),
    father_name nvarchar(200),
    mother_name nvarchar(200),
    father_address nvarchar(500),
    mother_address nvarchar(500),
    father_job nvarchar(100),
    mother_job nvarchar(100),
    father_phone nvarchar(20),
    mother_phone nvarchar(20),
    status int,
    class_id int
);
create table teacher(
	teacher_id int not null primary key auto_increment,
    teacher_code nvarchar(20),
    teacher_name nvarchar(200),
    teacher_password nvarchar(500),
    teacher_avatar nvarchar(500),
    teacher_gender int,
    teacher_birth timestamp,
    teacher_address nvarchar(500),
    teacher_identify nvarchar(20),
    teacher_identify_address nvarchar(500),
    teacher_identify_date timestamp,
    teacher_phone nvarchar(20),
    teacher_email nvarchar(200),
    teacher_time_table nvarchar(2000),
    status int
);
create table administrator(
	admin_id int not null primary key auto_increment,
    admin_code nvarchar(20),
    admin_name nvarchar(200),
    admin_password nvarchar(500),
    admin_avatar nvarchar(500),
    admin_gender int,
    admin_birth timestamp,
    admin_address nvarchar(500),
    admin_identify nvarchar(20),
    admin_identify_address nvarchar(500),
    admin_identify_date timestamp,
    admin_phone nvarchar(20),
    admin_email nvarchar(200),
    admin_role int,
	status int
);
create table class(
	class_id int not null primary key auto_increment,
    class_code nvarchar(100),
    class_name nvarchar(100),
    class_teacher int,
    class_status int,
    grade int,
    class_timetable nvarchar(1000),
    status int
);
create table semester(
	sem_id int not null primary key auto_increment,
    sem_code nvarchar(100),
    sem_name nvarchar(100),
    sem_year nvarchar(100),
    sem_start timestamp,
    sem_end timestamp
);
create table student_semester(
	student_semester_id int not null primary key auto_increment,
    class_id int,
    student_id int,
    sem_id int,
    table_mark nvarchar(2000),
    finish_mark nvarchar(1000)
);

create table subjects(
	subject_id int not null primary key auto_increment,
    subject_code nvarchar(200),
    subject_name nvarchar(200),
    subject_final int default 1
);

create table teacher_subject(
	teacher_subject_id int not null primary key auto_increment,
    teacher_id int,
    subject_id int
);

create table question(
	question_id int not null primary key auto_increment,
    question_code nvarchar(200),
    question_content nvarchar(2000),
    question_image nvarchar(1000),
    question_answers nvarchar(2000),
    subject_part_id int,
    subject_id int,
    question_level int
);

create table exam(
	exam_id int not null primary key auto_increment,
    exam_code nvarchar(200),
    exam_name nvarchar(200),
    exam_time bigint,
    exam_type int,
    exam_grade int,
    exam_subject int,
    teacher_id int
);

create table class_exam(
	class_exam_id int not null primary key auto_increment,
    class_id int,
    exam_id int,
    expire_date timestamp,
    type_mark int,
    max_times int
);

create table student_exam(
	student_exam_id int not null primary key auto_increment,
    student_id int,
    exam_id int,
    start_time timestamp,
    exam_random nvarchar(2000),
    exam_result nvarchar(1000),
    time_finish timestamp,
    result_temp nvarchar(1000),
    mark double,
    status int
);

create table subject_part(
	subject_part_id int not null primary key auto_increment,
    subject_part_code nvarchar(100),
    subject_part_name nvarchar(200),
    subject_id int
);

create table exam_question(
	exam_question_id int not null primary key auto_increment,
    exam_id int,
    question_id int
);

alter table class add constraint fk_class_teacher foreign key (class_teacher) references teacher(teacher_id);
alter table class_exam add constraint fk_class_exam_class foreign key (class_id) references class(class_id);
alter table class_exam add constraint fk_class_exam_exam foreign key (exam_id) references exam(exam_id);
alter table exam add constraint fk_exam_subject foreign key (exam_subject) references subjects(subject_id);
alter table exam add constraint fk_exam_teacher foreign key (teacher_id) references teacher(teacher_id);
alter table exam_question add constraint fk_exam_question_exam foreign key (exam_id) references exam(exam_id);
alter table exam_question add constraint fk_exam_question_question foreign key (question_id) references question(question_id);
alter table question add constraint fk_question_subject_part foreign key (subject_part_id) references subject_part(subject_part_id);
alter table student_exam add constraint fk_student_exam_student foreign key (student_id) references student(student_id);
alter table student_exam add constraint fk_student_exam_exam foreign key (exam_id) references exam(exam_id);
alter table student_semester add constraint fk_student_semester_student foreign key (student_id) references student(student_id);
alter table student_semester add constraint fk_student_semester_class foreign key (class_id) references class(class_id);
alter table student_semester add constraint fk_student_semester_sem foreign key (sem_id) references semester(sem_id);
alter table subject_part add constraint fk_subject_part_subject foreign key (subject_id) references subjects(subject_id);
alter table teacher_subject add constraint fk_teacher_subject_teacher foreign key (teacher_id) references teacher(teacher_id);
alter table teacher_subject add constraint fk_teacher_subject_subject foreign key (subject_id) references subjects(subject_id);
alter table student add constraint fk_student_class foreign key (class_id) references class(class_id);
alter table question add constraint fk_question_subject foreign key (subject_id) references subjects(subject_id);