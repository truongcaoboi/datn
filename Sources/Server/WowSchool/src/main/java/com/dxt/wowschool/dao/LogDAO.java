package com.dxt.wowschool.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.dxt.wowschool.InfoSystem;
import com.dxt.wowschool.packet.PacketLogin;
import com.dxt.wowschool.packet.ReceiveLogin;
import com.dxt.wowschool.vo.Human;
import com.dxt.wowschool.vo.Student;

@Component
public class LogDAO {
	public PacketLogin login(ReceiveLogin login) {
		PacketLogin pk = new PacketLogin();
		String username = login.getUsername();
		String password = login.getPassword();
		String sql = "";
		int type = -1;
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet res = null;
		try {
			if (username == null) {
				return new PacketLogin();
			}
			if (username.trim().equals("")) {
				return new PacketLogin();
			}
			if (password == null) {
				return new PacketLogin();
			}
			if (password.trim().equals("")) {
				return new PacketLogin();
			}
			if (username.startsWith("AD")) {
				sql = "select admin_id as id, admin_code as code, admin_name as name, admin_avatar as avatar, admin_gender as gender "
						+ " from administrator where admin_code = ? and admin_password = ? and status = 1";
				type = InfoSystem.CODE_ADMIN;
			} else if (username.startsWith("TE")) {
				sql = "select teacher_id as id, teacher_code as code, teacher_name as name, teacher_avatar as avatar, teacher_gender as gender "
						+ " from teacher where teacher_code = ? and teacher_password = ? and status = 1";
				type = InfoSystem.CODE_TEACHER;
			} else if (username.startsWith("ST")) {
				sql = "select student_id as id, student_code as code, student_name as name, student_avatar as avatar, student_gender as gender "
						+ " from student where student_code = ? and student_password = ? and status = 1";
				type = InfoSystem.CODE_STUDENT;
			}
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setString(1, username);
			state.setString(2, password);
			res = state.executeQuery();
			while(res.next()) {
				pk.setIdUser(res.getInt("id"));
				pk.setFullNameUser(res.getString("name"));
				pk.setUsername(res.getString("code"));
				pk.setUsertype(type);
				pk.setStatus(1);
				pk.setToken(InfoSystem.addUserLogins(res.getInt("id"), type));
				pk.setGender(res.getInt("gender"));
				pk.setAvatar(res.getString("avatar"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			MysqlConnector.close(res, null, state, conn);
		}
		return pk;
	}
	
	public Map<String,Object> validateAccount(String code, String identify){
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet res = null;
		Map map = new HashMap<>();
		map.put("status", 0);
		try {
			String sql = "";
			int type = -1;
			if (code.startsWith("AD")) {
				sql = "select admin_id as id, admin_code as code, admin_name as name, admin_avatar as avatar "
						+ " from administrator where admin_code = ? and admin_identify = ? and status = 1";
				type = InfoSystem.CODE_ADMIN;
			} else if (code.startsWith("TE")) {
				sql = "select teacher_id as id, teacher_code as code, teacher_name as name, teacher_avatar as avatar "
						+ " from teacher where teacher_code = ? and teacher_identify = ? and status = 1";
				type = InfoSystem.CODE_TEACHER;
			} else if (code.startsWith("ST")) {
				sql = "select student_id as id, student_code as code, student_name as name, student_avatar as avatar "
						+ " from student where student_code = ? and student_identify = ? and status = 1";
				type = InfoSystem.CODE_STUDENT;
			}
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setString(1, code);
			state.setString(2, identify);
			res = state.executeQuery();
			while(res.next()) {
				map.put("status", 1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MysqlConnector.close(res, null, state, conn);
		}
		return map;
	}
	
	public Map<String,Object> changePassword(String code, String password){
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet res = null;
		Map map = new HashMap<>();
		map.put("status", 0);
		try {
			String sql = "";
			int type = -1;
			if (code.startsWith("AD")) {
				sql = "update administrator set admin_password = ? where admin_code = ? and status = 1";
				type = InfoSystem.CODE_ADMIN;
			} else if (code.startsWith("TE")) {
				sql = "update teacher set teacher_password = ? where teacher_code = ? and status = 1";
				type = InfoSystem.CODE_TEACHER;
			} else if (code.startsWith("ST")) {
				sql = "update student set student_password = ? where student_code = ? and status = 1";
				type = InfoSystem.CODE_STUDENT;
			}
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setString(1, password);
			state.setString(2, code);
			int upd = state.executeUpdate();
			if(upd!=0) {
				map.put("status", 1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MysqlConnector.close(res, null, state, conn);
		}
		return map;
	}
}
