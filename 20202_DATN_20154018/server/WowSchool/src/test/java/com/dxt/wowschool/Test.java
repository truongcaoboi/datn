package com.dxt.wowschool;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.assertj.core.presentation.HexadecimalRepresentation;

import com.dxt.wowschool.dao.MysqlConnector;
import com.dxt.wowschool.dao.SemesterDAO;
import com.dxt.wowschool.util.Util;

public class Test {
	public static void main(String args[]) {
//		createRandomTableMark();
//		SemesterDAO dao = new SemesterDAO();
//		double [] a = new double []{-1,		1,-1,-1,-1,-1		,1,1,-1,-1,-1		,1,1,-1,-1,		5,-1};
//		System.out.println(dao.getSubjectMarkFinalByArray(a));
//		System.out.println(testHash("dxt"));
	}
	
	private static void createRandomTableMark() {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			conn = MysqlConnector.getMysqlConnector();
			List<Integer> ids = new ArrayList<Integer>();
			List<double[][]> marks = new ArrayList<double[][]>();
			String sql = "select * from student_semester";
			state = conn.prepareStatement(sql);
			rs = state.executeQuery();
			while(rs.next()) {
				ids.add(rs.getInt("student_semester_id"));
				marks.add(Util.gson.fromJson(rs.getString("table_mark"), double[][].class));
			}
			//mark 16
			Random rd = new Random();
			boolean checkHasLineFinal = false;
			for(int i = 0;i<ids.size();i++) {
				int id = ids.get(i);
				double[][] mss = marks.get(i);
				List<double[]> msNew = new ArrayList<>();
				for(int j = 0;j<mss.length;j++) {
					double[] ms1 = mss[j];
					if(ms1[0] == -1) {
						checkHasLineFinal = true;
					}
					double[] ms = new double[]{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};
					ms[0] = ms1[0];
					if(ms[0] == -1) {
						msNew.add(ms);
						continue;
					}
					ms[1] = rd.nextInt(11);
					ms[2] = rd.nextInt(11);
					ms[3] = rd.nextInt(11);
					if(rd.nextBoolean()) {
						ms[4] = rd.nextInt(11);
						if(rd.nextBoolean()) {
							ms[5] = rd.nextInt(11);
						}else {
							ms[5] = -1;
						}
					}else {
						ms[4] = -1;
						ms[5] = -1;
					}
					int r = rd.nextInt(3);
					ms[6] = rd.nextInt(10)+((r==1f?1:r==1?0.25f:r==2?0.5f:0.75f));
					r = rd.nextInt(3);
					ms[7] = rd.nextInt(10)+((r==1f?1:r==1?0.25f:r==2?0.5f:0.75f));
					r = rd.nextInt(3);
					ms[8] = rd.nextInt(10)+((r==1f?1:r==1?0.25f:r==2?0.5f:0.75f));
					ms[9] = -1;
					ms[10] = -1;
					r = rd.nextInt(3);
					ms[11] = rd.nextInt(10)+((r==1f?1:r==1?0.25f:r==2?0.5f:0.75f));
					r = rd.nextInt(3);
					ms[12] = rd.nextInt(10)+((r==1f?1:r==1?0.25f:r==2?0.5f:0.75f));
					ms[13] = -1;
					ms[14] = -1;
					r = rd.nextInt(3);
					ms[15] = rd.nextInt(10)+((r==1f?1:r==1?0.25f:r==2?0.5f:0.75f));
					msNew.add(ms);
				}
				if(!checkHasLineFinal) {
					double[] ms = new double[]{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};
					msNew.add(ms);
				}
				state.close();
				sql = "update student_semester set table_mark = ? where student_semester_id = ?";
				state = conn.prepareStatement(sql);
				state.setString(1, Util.gson.toJson(msNew));
				state.setInt(2, id);
				state.executeUpdate();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		System.out.println("Ok");
	}
	
//	public static String testHash(String strTest) {
//		try {
//			MessageDigest digest = MessageDigest.getInstance("SHA-256");
//			byte[] hash = digest.digest(
//					strTest.getBytes(StandardCharsets.UTF_8));
//			String sha256hex = new String(Hex);
//			return sha256hex;
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
//		return null;
//	}
}
