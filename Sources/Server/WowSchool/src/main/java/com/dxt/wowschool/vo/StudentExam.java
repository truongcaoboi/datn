package com.dxt.wowschool.vo;

import java.util.Date;

public class StudentExam {
	private int id;
	private int studentId;
	private int examId;
	private Date startTime;
	private String examRandom;
	private String result;
	private Date timeFinish;
	private String resultTemp;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getStudentId() {
		return studentId;
	}
	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}
	public int getExamId() {
		return examId;
	}
	public void setExamId(int examId) {
		this.examId = examId;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public String getExamRandom() {
		return examRandom;
	}
	public void setExamRandom(String examRandom) {
		this.examRandom = examRandom;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public Date getTimeFinish() {
		return timeFinish;
	}
	public void setTimeFinish(Date timeFinish) {
		this.timeFinish = timeFinish;
	}
	public String getResultTemp() {
		return resultTemp;
	}
	public void setResultTemp(String resultTemp) {
		this.resultTemp = resultTemp;
	}
	
	
}
