package com.dxt.wowschool;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.dxt.wowschool.util.Util;

public class InfoSystem {
	public static final int CODE_STUDENT = 1;
	public static final int CODE_TEACHER = 2;
	public static final int CODE_ADMIN = 3;
	public static final int GENDER_NAM = 1;
	public static final int GENDER_NU = 2;
	public static Map<String, String> userLogins = new HashMap<String, String>();
	
	public static String genKey(int id, int usertype) {
		String key = "";
		if(usertype == CODE_ADMIN) {
			key+= "AD";
		}else if(usertype == CODE_TEACHER) {
			key += "TE";
		}else if(usertype == CODE_STUDENT) {
			key += "ST";
		}
		key += id;
		return key;
	}
	
	public static boolean isLogin(int id, int usertype, String token) {
		try {
			String key = genKey(id, usertype);
			for(String mapKey : userLogins.keySet()) {
				if(key.equals(mapKey)) {
					if(token.equals(userLogins.get(mapKey))) {
						return true;
					}else {
						userLogins.remove(mapKey);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public static String addUserLogins(int id, int usertype) {
		try {
			String key = genKey(id, usertype);
			String uuid = UUID.randomUUID().toString();
			if(userLogins.containsKey(key)) {
				userLogins.remove(key);
			}
			userLogins.put(key, uuid);
			return uuid;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static void deleteUserLogin(int id, int usertype) {
		try {
			String key = genKey(id, usertype);
			String uuid = UUID.randomUUID().toString();
			if(userLogins.containsKey(key)) {
				userLogins.remove(key);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
