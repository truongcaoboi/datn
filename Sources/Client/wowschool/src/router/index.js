import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Login from '@/components/common/Login'
import AdminPage from '@/components/admin/AdminPage'
import StudentPage from '@/components/student/StudentPage'
import TeacherPage from '@/components/teacher/TeacherPage'
import InfoUserPage from '@/components/common/InfoUser'

import MAdmin from '@/components/admin/MAdmin'
import MTeacher from '@/components/admin/MTeacher'
import MStudent from '@/components/admin/MStudent'
import MClass from '@/components/admin/MClass'
import MSemester from '@/components/admin/MSemester'
import MSubject from '@/components/admin/MSubject'

import MTeacherTKB from '@/components/teacher/MTimeTable'
import MTeacherClass from '@/components/teacher/MClassT'
import MMarkStudent from '@/components/teacher/MMarkStudent'
import MQuestion from '@/components/teacher/MQuestion'
import MTeacherExam from '@/components/teacher/MExam'

import MStudentTKB from '@/components/student/MStudentTKB'
import MStudentClass from '@/components/student/MClassS'
import MMark from '@/components/student/MMyMark'
import MStudentExam from '@/components/student/MExam'
import MStudentHistory from "@/components/student/History"

import Exam from "@/components/student/Exam"

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/admin',
      name: 'AdminMainPage',
      component:AdminPage,
      children:[{
        path: '/admin/madmin',
        name: 'MAdmin',
        components:{
          MAdmin: MAdmin
        }
      },{
        path: '/admin/mteacher',
        name: 'MTeacher',
        components:{
          MTeacher: MTeacher
        }
      },{
        path: '/admin/mstudent',
        name: 'MStudent',
        components:{
          MStudent: MStudent
        }
      },{
        path: '/admin/mclass',
        name: 'MClass',
        components:{
          MClass: MClass
        }
      },{
        path: '/admin/msemester',
        name: 'MSemester',
        components:{
          MSemester: MSemester
        }
      },{
        path: '/admin/msubject',
        name: 'MSubject',
        components:{
          MSubject: MSubject
        }
      }]
    },
    {
      path: '/student',
      name: 'StudentMainPage',
      component: StudentPage,
      children:[{
        path: '/student/mstudenttkb',
        name: 'MStudentTKB',
        components:{
          MStudentTKB: MStudentTKB
        }
      },{
        path: '/student/mstudentclass',
        name: 'MStudentClass',
        components:{
          MStudentClass: MStudentClass
        }
      },{
        path: '/student/mmymark',
        name: 'MMark',
        components:{
          MMark: MMark
        }
      },{
        path: '/student/mstudentexam',
        name: 'MStudentExam',
        components:{
          MStudentExam: MStudentExam
        }
      },{
        path: '/student/mstudenthistory',
        name: 'MStudentHistory',
        components:{
          MStudentHistory: MStudentHistory
        }
      }]
    },
    {
      path: '/teacher',
      name: 'TeacherMainPage',
      component: TeacherPage,
      children:[{
        path: '/teacher/mteachertkb',
        name: 'MTeacherTKB',
        components:{
          MTeacherTKB: MTeacherTKB
        }
      },{
        path: '/teacher/mteacherclass',
        name: 'MTeacherClass',
        components:{
          MTeacherClass: MTeacherClass
        }
      },{
        path: '/teacher/mmarkstudent',
        name: 'MMarkStudent',
        components:{
          MMarkStudent: MMarkStudent
        }
      },{
        path: '/teacher/mquestion',
        name: 'MQuestion',
        components:{
          MQuestion: MQuestion
        }
      },{
        path: '/teacher/mteacherexam',
        name: 'MTeacherExam',
        components:{
          MTeacherExam: MTeacherExam
        }
      }]
    },
    {
      path: '/info',
      name: 'InfoUserPage',
      component: InfoUserPage,
    },
    {
      path: '/exam',
      name: 'exam',
      component: Exam,
    }
  ]
})
