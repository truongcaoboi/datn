package com.dxt.wowschool.packet;

import java.util.ArrayList;
import java.util.List;

import com.dxt.wowschool.vo.ClassRoom;
import com.dxt.wowschool.vo.Student;
import com.dxt.wowschool.vo.Teacher;

public class PacketClassInfo {
	private List<Student> students = new ArrayList<Student>();
	private ClassRoom cls = new ClassRoom();
	private Teacher teacher = new Teacher();
	public List<Student> getStudents() {
		return students;
	}
	public void setStudents(List<Student> students) {
		this.students = students;
	}
	public ClassRoom getCls() {
		return cls;
	}
	public void setCls(ClassRoom cls) {
		this.cls = cls;
	}
	public Teacher getTeacher() {
		return teacher;
	}
	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}
	
	
}
