package com.dxt.wowschool.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

import com.dxt.wowschool.util.Util;
import com.dxt.wowschool.vo.ClassRoom;
import com.dxt.wowschool.vo.Semester;
import com.dxt.wowschool.vo.TableMark;

@Component
public class SemesterDAO {
	public List<Semester> getAllSemester() {
		List<Semester> data = new ArrayList<>();
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			String sql = "select * from semester order by sem_id DESC";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			rs = state.executeQuery();
			while (rs.next()) {
				Semester sem = new Semester();
				sem.setId(rs.getInt("sem_id"));
				sem.setCode(rs.getString("sem_code"));
				sem.setName(rs.getString("sem_name"));
				sem.setYear(rs.getString("sem_year"));
				data.add(sem);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return data;
	}
	
	public Semester getSemesterById(int id) {
		Semester sem = null;
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			String sql = "select * from semester where sem_id = ?";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setInt(1, id);
			rs = state.executeQuery();
			while (rs.next()) {
				sem = new Semester();
				sem.setId(rs.getInt("sem_id"));
				sem.setCode(rs.getString("sem_code"));
				sem.setName(rs.getString("sem_name"));
				sem.setYear(rs.getString("sem_year"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return sem;
	}

	public int updateSemester(Semester sem) {
		int res = -1;
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			String sql = "update semester set " + "sem_name = ?, sem_year = ? where sem_id = ?";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setString(1, sem.getName());
			state.setString(2, sem.getYear());
			state.setInt(3, sem.getId());
			res = state.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return res;
	}

	public String getNextCode() {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		String code = "";
		try {
			String sql = "select max(sem_id) as id from semester";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			rs = state.executeQuery();
			int max = 0;
			while (rs.next()) {
				max = rs.getInt("id");
			}
			max++;
			code = "SEM" + Util.addPrefix(max + "", 4, '0');
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return code;
	}

	public int addSemester(Semester sem) {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		int res = -1;
		try {
			String sql = "insert into semester(sem_code,sem_name,sem_year) values (?,?,?)";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setString(1, sem.getCode());
			state.setString(2, sem.getName());
			state.setString(3, sem.getYear());
			res = state.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return res;
	}

	public int deleteSemester(int id) {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		int res = -1;
		try {
			String sql = "delete from student_semester where sem_id = ?";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setInt(1, id);
			state.execute();
			sql = "delete from semester where sem_id = ?";
			state = conn.prepareStatement(sql);
			state.setInt(1, id);
			state.execute();
			res = 1;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return res;
	}

	public Map<String, Object> addClass(ClassRoom cl, int sem_id) {
		int res = -1;
		String mes = "Thêm thất bại!";
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("status", res);
		map.put("mes", mes);
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			String sql = "select max(sem_id) as id from semester";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			rs = state.executeQuery();
			while (rs.next()) {
				sem_id = rs.getInt("id");
			}
			if (sem_id > 0) {
				sql = "select count(*) as count from student_semester where sem_id = ? and class_id = ?";
				state = conn.prepareStatement(sql);
				state.setInt(1, sem_id);
				state.setInt(2, cl.getId());
				rs = state.executeQuery();
				int count = 0;
				while (rs.next()) {
					count = rs.getInt("count");
				}
				if (count == 0) {
					List<Integer> ids = new ArrayList<>();
					sql = "select student_id as id from student where class_id = ?";
					state = conn.prepareStatement(sql);
					state.setInt(1, cl.getId());
					rs = state.executeQuery();
					while (rs.next()) {
						ids.add(rs.getInt("id"));
					}
					for (int id : ids) {
						sql = "insert into student_semester(class_id, student_id, sem_id, table_mark) values(?,?,?,?)";
						state = conn.prepareStatement(sql);
						state.setInt(1, cl.getId());
						state.setInt(2, id);
						state.setInt(3, sem_id);
						TableMark t = new TableMark();
						t.createTableMark();
						state.setString(4, Util.gson.toJson(t.getSubjectMarks()));
						state.executeUpdate();
					}
					res = 1;
					map.put("status", 1);
				} else {
					mes = "Lớp đã được thêm trước đó!";
					map.put("mes", mes);
				}
			} else {
				mes = "Chưa có kỳ học nào!";
				map.put("mes", mes);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return map;
	}
	
	public int tongket(int semid) {
		int result = -1;
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			String sql = "select * from student_semester where sem_id = ?";
			List<Integer> ids = new ArrayList<>();
			List<Integer> idSubjects = new ArrayList<>();
			List<double[][]> tableMarks = new ArrayList<>();
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setInt(1, semid);
			rs = state.executeQuery();
			while(rs.next()) {
				ids.add(rs.getInt("student_semester_id"));
				tableMarks.add(Util.gson.fromJson(rs.getString("table_mark"), double[][].class));
			}
			sql = "select * from subjects where subject_final = 1";
			state.close();
			state = conn.prepareStatement(sql);
			rs.close();
			rs = state.executeQuery();
			while(rs.next()) {
				idSubjects.add(rs.getInt("subject_id"));
			}
			for(int i = 0;i<ids.size();i++) {
				int id = ids.get(i);
				double [][] tableMark = tableMarks.get(i);
				for(double[] marks : tableMark) {
					marks[16] = getSubjectMarkFinalByArray(marks);
				}
				tableMark[tableMark.length - 1][16] = getMarkFinal(tableMark, idSubjects);
				state.close();
				sql = "update student_semester set table_mark = ? where student_semester_id = ?";
				state = conn.prepareStatement(sql);
				state.setString(1, Util.gson.toJson(tableMark));
				state.setInt(2, id);
				result = state.executeUpdate();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return result;
	}
	
	public double getSubjectMarkFinalByArray(double[] marks) {
		double result = 0;
		int count = 0;
		int countItem = 0;
		//diem he so 3
		if(marks[15] >= 0) {
			count += 3;
			result += marks[15] * 3;
			//diem he so 2
			for(int i = 11;i<15;i++) {
				if(marks[i] >= 0) {
					count += 2;
					countItem++;
					result += marks[i] * 2;
				}
			}
			if(countItem > 0) {
				//diem 15'
				countItem = 0;
				for(int i = 6;i<11;i++) {
					if(marks[i] >= 0) {
						countItem ++;
						count ++;
						result += marks[i];
					}
				}
				if(countItem > 0) {
					//diem mieng
					for(int i = 1;i<6;i++) {
						if(marks[i] >= 0) {
							count++;
							result += marks[i];
						}
					}
				}else {
					result = -1;
				}
			}else {
				result = -1;
			}
		}else {
			result = -1;
		}
		if(result >= 0 & count > 0) {
			result = Math.floor(result/count*100)*(1.0)/100;
		}
		return result;
	}

	public double getMarkFinal(double[][] tableMark, List<Integer> idSubjects) {
		double result = 0;
		int count = 0;
		for(double[] marks : tableMark) {
			if(marks[0] > 0) {
				if(idSubjects.contains((int)marks[0])) {
					if(marks[16] >= 0) {
						result += marks[16];
						count++;
					}
				}
			}
		}
		if(count>0 & count == idSubjects.size()) {
			result = Math.floor(result/count*100)*(1.0)/100;
		}else {
			result = -1;
		}
		return result;
	}
}
