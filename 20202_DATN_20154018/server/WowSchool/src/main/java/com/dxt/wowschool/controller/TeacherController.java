package com.dxt.wowschool.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dxt.wowschool.dao.TeacherDAO;
import com.dxt.wowschool.vo.ClassRoom;
import com.dxt.wowschool.vo.Subject;
import com.dxt.wowschool.vo.Teacher;

@RestController
@RequestMapping("/api/teachers")
public class TeacherController {
	@Autowired
	private TeacherDAO teacherDao;
	@GetMapping("/allTeacher")
	public List<Teacher> getAllTeacher(){
		return teacherDao.getTeachers();
	}
	
	@GetMapping("/nextCode")
	public String getNextCode() {
		return teacherDao.getNewTeacherCode();
	}
	
	@GetMapping("/getbyid/{id}")
	public Teacher getTeacherById(@PathVariable int id) {
		System.out.println(id);
		Teacher admin = teacherDao.getTeacher(id);
		return admin;
	}
	
	@PostMapping("/addTeacher")
	public Map<String,Object> addTeacher(@RequestBody Teacher ad){
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("status", 0);
		try {
			Teacher a = teacherDao.addTeacher(ad);
			if(a != null) {
				map.put("status", 1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	@PutMapping("/updateInfo")
	public Map<String,Object> updateInfo(@RequestBody Teacher ad){
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("status", 0);
		try {
			map.put("status", teacherDao.updateTeacher(ad));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	@DeleteMapping("/deleteTeacher/{id}")
	public Map<String,Object> deleteTeacher(@PathVariable int id){
		Map<String,Object> map = new HashMap<>();
		map.put("status", 0);
		map.put("status", teacherDao.deleteTeacher(id));
		return map;
	}
	
	@GetMapping("/seach")
	public List<Teacher> search(@RequestParam String content){
		return teacherDao.searchTeacher(content, content);
	}
	
	@GetMapping("/getClassTeach/{id}")
	public List<ClassRoom> getClassTeach(@PathVariable int id){
		return teacherDao.getClassTeach(id);
	}
	
	@GetMapping("/getSubjectWithClassTeach/{id}/{idClass}")
	public List<Subject> getSubjectWithClassTeach(@PathVariable int id,@PathVariable int idClass){
		return teacherDao.getSubjectWithClassTeach(id, idClass);
	}
}
