-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: wowschooltest
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `question` (
  `question_id` int NOT NULL AUTO_INCREMENT,
  `question_code` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `question_content` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `question_image` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `question_answers` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `subject_part_id` int DEFAULT NULL,
  `question_level` int DEFAULT NULL,
  `subject_id` int DEFAULT NULL,
  PRIMARY KEY (`question_id`),
  KEY `fk_question_subject_part` (`subject_part_id`),
  CONSTRAINT `fk_question_subject_part` FOREIGN KEY (`subject_part_id`) REFERENCES `subject_part` (`subject_part_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
INSERT INTO `question` VALUES (1,'QUE0001','Chọn câu đúng','/resource/images/234a7196-445f-4f02-ad5e-04d0b47b756dimages.jfif','0##dxt##1+1=2##dxt##1+1=3##dxt##1+1=4##dxt##1+1=5',3,1,1),(2,'QUE0002','Đặc điểm nổi bật của nền kinh tế Mĩ những năm đầu sau Chiến tranh thế giới thứ hai là','','0##dxt##bị thiệt hại nặng nề về người và của do hậu quả của Chiến tranh thế giới thứ hai.##dxt##có tốc độ phát triển mạnh mẽ và chiếm hơn 70% sản lượng công nghiệp thế giới.##dxt##bị suy giảm nghiêm trọng do phải chi phí cho sản xuất vũ khí, chạy đua vũ trang.##dxt##phát triển mạnh mẽ, trở thành trung tâm kinh tế - tài chính lớn, duy nhất thế giới.',12,-1,7),(3,'QUE0003','Từ thực tiễn phong trào yêu nước (1919 - 1925) của lực lượng tiểu tư sản, trí thức Việt Nam có thể rút ra biện pháp nào sau đây để phát huy vai trò của đội ngũ trí thức trong sự nghiệp công nghiệp hóa, hiện đại hóa đất nước?','','0##dxt##Đẩy mạnh cải cách hành chính, tăng tính cạnh tranh của nền kinh tế.##dxt##Bồi dưỡng nguồn nhân lực chất lượng cao để nhạy bén với thời cuộc.##dxt##Đưa đội ngũ trí thức tham gia vào các cơ quan, bộ máy của Nhà nước.##dxt##Quốc hội ban hành luật đầu tư cho đội ngũ trí thức được làm kinh tế.',12,-1,7),(4,'QUE0004','Nhận xét nào sau đây là đúng về phong trào công nhân Việt Nam trong những năm 1928 -1929?','','0##dxt##Có tính thống nhất cao theo một đường lối chính trị đúng đắn tử đầu.##dxt##Giai cấp công nhân đã trưởng thành, đủ sức lãnh đạo cuộc cách mạng.##dxt##Phát triển ngày càng mạnh mẽ và có một tổ chức lãnh đạo thống nhất.##dxt##Có sự liên kết chặt chẽ và trở thành nòng cốt của phong trào dân tộc.',12,-1,7),(5,'QUE0005','Nguyên nhân khách quan thúc đẩy kinh tế Nhật phát triển là','','0##dxt##Có tính thống nhất cao theo một đường lối chính trị đúng đắn tử đầu.##dxt##Chiến tranh thế giới thứ hai kết thúc đã đem lại cho Nhật nhiều nguồn lợi.##dxt##biết tận dụng thành tựu khoa học – kĩ thuật của thế giới.##dxt##con người Nhật Bản có ý thức vươn lên, được đào tạo trình độ cao, cần cù lao động.',12,-1,7),(6,'QUE0006','Hiện nay Việt Nam đã hội nhập vào xu thế toàn cầu hóa bằng việc trở thành thành viên của','','0##dxt##WTO, APEC.##dxt##UNESCO##dxt##UNICEF##dxt##NATO',12,-1,7),(9,'QUE0007','Các cấp tổ chức cơ bản của thế giới sống bao gồm: <br>\n1. Quần xã <br>2. Quần thể  <br>3. Cơ thể  <br>4. Hệ sinh thái <br>5. Tế bào  <br>Các cấp tổ chức đó theo trình tự từ nhỏ đến lớn là…','','1##dxt##5-3-2-4-1##dxt##5-3-2-1-4##dxt##5-2-3-1-4##dxt##5-2-3-4-1',96,1,6),(10,'QUE0010','Hãy chọn câu sau đây có thứ tự sắp xếp các cấp độ tổ chức sống từ thấp đến cao:','','3##dxt##Cơ thể, quần thể, hệ sinh thái, quần xã##dxt##Quần xã, quần thể, hệ sinh thái, cơ thể##dxt##Quần thể, quần xã, cơ thể, hệ sinh thái##dxt##Cơ thể, quần thể, quần xã, hệ sinh thái',96,1,6),(11,'QUE0011','Trong các cấp tổ chức sống dưới đây, cấp nào là lớn nhất ?','','1##dxt##Tế bào##dxt##Quần xã##dxt##Quần thể##dxt##Bào quan',96,1,6),(12,'QUE0012','Tổ chức sống nào sau đây có cấp thấp nhất so với các tổ chức còn lại?','','2##dxt##Quần thể##dxt##Quần xã##dxt##Cơ thể##dxt##Hệ sinh thái',96,1,6),(13,'QUE0013','\"Đàn voi sống trong rừng\" thuộc cấp độ tổ chức sống nào dưới đây?','','2##dxt##Cá thể##dxt##Quần xã##dxt##Quần thể##dxt##Hệ sinh thái',96,1,6);
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-29 17:22:43
