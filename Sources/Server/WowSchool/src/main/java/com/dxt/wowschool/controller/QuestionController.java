package com.dxt.wowschool.controller;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dxt.wowschool.dao.QuestionDAO;
import com.dxt.wowschool.vo.Question;

@RestController
@RequestMapping("/api/questions")
public class QuestionController {
	@Autowired
	private QuestionDAO dao;
	
	@GetMapping("/getQuestion")
	public List<Question> getQuestions(@RequestParam int subjectId, @RequestParam int subjectPartId, @RequestParam int levelId){
		return dao.getAllQuestionFilter(subjectId, subjectPartId, levelId);
	}
	
	@GetMapping("/newCode")
	public String getNewCode() {
		return dao.getNewCode();
	}
	
	@PostMapping("/addQuestion")
	public Map<String, Object> addQuestion(@RequestBody Question q){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("status", dao.addQuestion(q));
		return map;
	}
	
	@PutMapping("/updateQuestion")
	public Map<String, Object> updateQuestion(@RequestBody Question q){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("status", dao.update(q));
		return map;
	}
	@GetMapping("/getbyid/{id}")
	public Question getQuestionById(@PathVariable int id) {
		return dao.getQuestion(id);
	}
}
