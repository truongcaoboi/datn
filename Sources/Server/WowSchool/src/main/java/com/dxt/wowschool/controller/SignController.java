package com.dxt.wowschool.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dxt.wowschool.InfoSystem;
import com.dxt.wowschool.dao.LogDAO;
import com.dxt.wowschool.packet.PacketLogin;
import com.dxt.wowschool.packet.ReceiveLogin;
import com.dxt.wowschool.util.Util;
import com.google.gson.JsonObject;

@RestController
@RequestMapping("/api/sign")
public class SignController {
	@Autowired
	private LogDAO logDao;
	
	@PostMapping("/login")
	public PacketLogin login(@RequestBody ReceiveLogin login) {
		PacketLogin pk = logDao.login(login);
		return pk;
	}
	
	@PostMapping("/checkLogin")
	public Map<String,Object> checkLogin(@RequestBody Map<String,Object> checkLogin) {
		Map map = new HashMap<>();
		map.put("status", 0);
		try {
			boolean check = InfoSystem.isLogin((int)checkLogin.get("id")
					,(int) checkLogin.get("type"),(String) checkLogin.get("token"));
			if(check) {
				map.put("status", 1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	@PostMapping("/validateAccount")
	public Map<String,Object> validateAccount(@RequestBody Map<String,Object> validateAccount){
		Map map = new HashMap<>();
		map.put("status", 0);
		try {
			String code = (String) validateAccount.get("code");
			String identify = (String) validateAccount.get("identify");
			map = logDao.validateAccount(code, identify);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	@PostMapping("/changePassword")
	public Map<String,Object> changePassword(@RequestBody Map<String,Object> changePackage){
		Map map = new HashMap<>();
		map.put("status", 0);
		try {
			String code = (String) changePackage.get("code");
			String password = (String) changePackage.get("password");
			map = logDao.changePassword(code, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	@PostMapping("/logout")
	public Map<String,Object> logout(@RequestBody Map<String,Object> logout){
		Map map = new HashMap<>();
		map.put("status", 0);
		try {
			int id = (int) logout.get("id");
			int type = (int) logout.get("type");
			InfoSystem.deleteUserLogin(id, type);
			map.put("status", 1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
}
