package com.dxt.wowschool.vo;

public class Student extends Human{
	private String fatherName;
	private String motherName;
	private String fatherAddress;
	private String motherAddress;
	private String fatherJob;
	private String motherJob;
	private String fatherPhone;
	private String motherPhone;
	private int classId;
	public String getFatherName() {
		return fatherName;
	}
	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}
	public String getMotherName() {
		return motherName;
	}
	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}
	public String getFatherAddress() {
		return fatherAddress;
	}
	public void setFatherAddress(String fatherAddress) {
		this.fatherAddress = fatherAddress;
	}
	public String getMotherAddress() {
		return motherAddress;
	}
	public void setMotherAddress(String motherAddress) {
		this.motherAddress = motherAddress;
	}
	public String getFatherJob() {
		return fatherJob;
	}
	public void setFatherJob(String fatherJob) {
		this.fatherJob = fatherJob;
	}
	public String getMotherJob() {
		return motherJob;
	}
	public void setMotherJob(String motherJob) {
		this.motherJob = motherJob;
	}
	public String getFatherPhone() {
		return fatherPhone;
	}
	public void setFatherPhone(String fatherPhone) {
		this.fatherPhone = fatherPhone;
	}
	public String getMotherPhone() {
		return motherPhone;
	}
	public void setMotherPhone(String motherPhone) {
		this.motherPhone = motherPhone;
	}
	public int getClassId() {
		return classId;
	}
	public void setClassId(int classId) {
		this.classId = classId;
	}
	
}
