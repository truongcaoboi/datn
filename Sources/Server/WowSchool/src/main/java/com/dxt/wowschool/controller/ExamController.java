package com.dxt.wowschool.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dxt.wowschool.dao.ExamDAO;
import com.dxt.wowschool.packet.PacketStudentExam;
import com.dxt.wowschool.vo.Exam;
import com.dxt.wowschool.vo.HistoryExam;

@RestController
@RequestMapping("/api/exams")
public class ExamController {
	@Autowired
	private ExamDAO dao;
	
	@GetMapping("/getExamById/{id}")
	public Exam getExamById(@PathVariable int id) {
		return dao.getExamById(id);
	}
	
	@GetMapping("/getAllExamFilter/{type}")
	public List<Exam> getExamFilter(@RequestParam int subjectId, @RequestParam int grade, @PathVariable int type){
		return dao.getExamFilter(subjectId, grade, type);
	}
	
	@GetMapping("/newCode")
	public String getNewCode() {
		return dao.getNewExamCode();
	}
	
	@PostMapping("/addExam")
	public Map<String,Object> addExam(@RequestBody Exam ex){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("status", dao.addExam(ex));
		return map;
	}
	
	@PutMapping("/updateExam/{id}")
	public Map<String,Object>  updateExam(@RequestBody Exam ex, @PathVariable int id){
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("status", dao.updateExam(ex,id));
		return map;
	}
	
	@GetMapping("/checkInExam/{id}")
	public Map<String,Object>  checkInExam(@PathVariable int id){
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("status", dao.checkInExam(id));
		return map;
	}
	
	@PostMapping("/createStudentExam")
	public Map<String,Object> createStudentExam(@RequestBody Map<String, Integer> body) {
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("status", dao.createStudentExam(body.get("studentId"), body.get("examId")));
		return map;
	}
	
	@GetMapping("/getStudentExam/{id}")
	public PacketStudentExam getStudentExam(@PathVariable int id) {
		return dao.getStudentExam(id);
	}
	
	@PostMapping("/updateStudentExam")
	public Map<String,Object> updateStudentExam(@RequestBody PacketStudentExam packet){
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("status", dao.updateStudentExam(packet));
		return map;
	}
	
	@GetMapping("/endStudentExam/{id}")
	public Map<String,Object> endStudentExam(@PathVariable int id){
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("status", dao.endStudentExam(id));
		return map;
	}
	@GetMapping("/getHistory/{id}")
	public List<HistoryExam> getHistoryExam(@PathVariable int id){
		return dao.getHistory(id);
	}
}
