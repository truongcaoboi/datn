package com.dxt.wowschool.vo;

public class Admin extends Human{
	public static final int NON_ADMIN = 0;
	public static final int IS_ADMIN = 1;
	
	private int role;

	public int getRole() {
		return role;
	}

	public void setRole(int role) {
		this.role = role;
	}
	
}
