package com.dxt.wowschool.controller;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dxt.wowschool.dao.SubjectDAO;
import com.dxt.wowschool.vo.Subject;
import com.dxt.wowschool.vo.SubjectPart;


@RestController
@RequestMapping("/api/subject")
public class SubjectController {
	@Autowired
	private SubjectDAO dao;
	
	@GetMapping("/allSubject")
	public List<Subject> getAllSubject(){
		return dao.getAllSubject();
	}
	
	@GetMapping("/getSubject/{id}")
	public Subject getSubjectById(@PathVariable int id) {
		return dao.getSubject(id);
	}
	
	@GetMapping("/getAllSubjectPart/{id}")
	public List<SubjectPart> getAllSubjectPart(@PathVariable int id){
		return dao.getAllSubjectPart(id);
	}
	
	@GetMapping("/getSubjectPart/{id}")
	public SubjectPart getSubjectPart(@PathVariable int id) {
		return dao.getSubjectPart(id);
	}
	
	@GetMapping("/nextSubjectCode")
	public String getNextSubjectCode() {
		return dao.getNewSubjectCode();
	}
	
	@GetMapping("/nextSubjectPartCode")
	public String getNextSubjectPartCode() {
		return dao.getNewSubjectPartCode();
	}
	
	@PostMapping("/addSubject")
	public Map<String, Object> addSubject(@RequestBody Subject sub){
		Map<String,Object> map = new HashMap<>();
		map.put("status", dao.addSubject(sub) != null ? 1 : 0);
		return map;
	}
	
	@PostMapping("/addSubjectPart")
	public Map<String, Object> addSubjectPart(@RequestBody SubjectPart subp){
		Map<String,Object> map = new HashMap<>();
		map.put("status", dao.addSubjectPart(subp)!= null ? 1: 0);
		return map;
	}
	
	@PutMapping("/updateSubject")
	public Map<String, Object> updateSubject(@RequestBody Subject sub){
		Map<String,Object> map = new HashMap<>();
		map.put("status", dao.updateSubject(sub));
		return map;
	}
	
	@PutMapping("/updateSubjectPart")
	public Map<String, Object> updateSubjectPart(@RequestBody SubjectPart subp){
		Map<String,Object> map = new HashMap<>();
		map.put("status", dao.updateSubjectPart(subp));
		return map;
	}
	
	@PostMapping("/addTeacherSubject")
	public Map<String, Object> updateSubjectPart(@RequestBody Map<String, Object> body){
		Map<String,Object> map = new HashMap<>();
		int teacherId = (int) body.get("teacherId");
		String subIds = (String) body.get("subIds");
		List<Integer> ids = new ArrayList<>();
		for(String id : subIds.split("#")) {
			ids.add(Integer.parseInt(id));
		}
		map.put("status", dao.addTeacherSubject(teacherId, ids));
		return map;
	}
}
