package com.dxt.wowschool.vo;

public class StudentSemester {
	private int id;
	private int studentId;
	private int classId;
	private int semId;
	private String markTable;
	private String finishMarkTable;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getStudentId() {
		return studentId;
	}
	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}
	public int getClassId() {
		return classId;
	}
	public void setClassId(int classId) {
		this.classId = classId;
	}
	public int getSemId() {
		return semId;
	}
	public void setSemId(int semId) {
		this.semId = semId;
	}
	public String getMarkTable() {
		return markTable;
	}
	public void setMarkTable(String markTable) {
		this.markTable = markTable;
	}
	public String getFinishMarkTable() {
		return finishMarkTable;
	}
	public void setFinishMarkTable(String finishMarkTable) {
		this.finishMarkTable = finishMarkTable;
	}
	
	
}
