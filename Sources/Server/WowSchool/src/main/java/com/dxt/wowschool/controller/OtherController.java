package com.dxt.wowschool.controller;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.dxt.wowschool.dao.OtherDAO;
import com.dxt.wowschool.util.Util;

@RestController
@RequestMapping("/api/others")
public class OtherController {
	@Autowired
	private OtherDAO dao;
	@PostMapping("/changeAvatar/{code}")
	public Map<String,Object> changeAvatar(@RequestParam MultipartFile file, @PathVariable String code){
		Map<String, Object> map = new HashMap<>();
		map.put("status", 0);
		try {
			String uri = ResourceUtils.getFile("classpath:static/images").getAbsolutePath();
			Path path = Paths.get(uri);
			String fileName = UUID.randomUUID().toString()+file.getOriginalFilename();
			Files.copy(file.getInputStream(), path.resolve(fileName), StandardCopyOption.REPLACE_EXISTING);
			String url = "/resource/images/"+fileName;
			map = dao.changeAvatar(code, url);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	@PostMapping("/changeImageQuestion/{id}")
	public Map<String,Object> changeImageQuestion(@RequestParam MultipartFile file, @PathVariable int id){
		Map<String, Object> map = new HashMap<>();
		map.put("status", 0);
		try {
			String uri = ResourceUtils.getFile("classpath:static/images").getAbsolutePath();
			Path path = Paths.get(uri);
			String fileName = UUID.randomUUID().toString()+file.getOriginalFilename();
			Files.copy(file.getInputStream(), path.resolve(fileName), StandardCopyOption.REPLACE_EXISTING);
			String url = "/resource/images/"+fileName;
			map = dao.changeImageQuestion(id, url);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

	@DeleteMapping("deleteImageQuestion/{id}")
	public Map<String,Object> deleteImageQuestion(@PathVariable int id){
		return dao.deleteImageQuestion(id);
	}
}
