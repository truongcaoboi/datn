package com.dxt.wowschool.controller;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dxt.wowschool.dao.ClassDAO;
import com.dxt.wowschool.vo.ClassRoom;

@RestController
@RequestMapping("/api/classes")
public class ClassController {
	@Autowired
	private ClassDAO dao;
	
	@GetMapping("/allClass")
	public List<ClassRoom> getAllClass(){
		return dao.getAllClassRoom();
	}
	
	@GetMapping("/allClass/{semId}")
	public List<ClassRoom> getAllClassBySem(@PathVariable int semId){
		return dao.getClassBySemester(semId);
	}
	
	@GetMapping("/allClassInSem")
	public List<ClassRoom> getAllClassInSem(){
		return dao.getClassInSemester();
	}
	
	@GetMapping("/allClassNotIn/{semId}")
	public List<ClassRoom> getAllClassNotInSem(@PathVariable int semId){
		return dao.getClassNotInSemester(semId);
	}
	
	@GetMapping("/getClass/{classId}")
	public ClassRoom getClassById(@PathVariable int classId) {
		return dao.getClassRoom(classId);
	}
	
	@GetMapping("/newCode")
	public String getNewCodeClass() {
		return dao.getNewClassCode();
	}
	
	@PostMapping("/addClass")
	public Map<String, Object> addClassRoom(@RequestBody ClassRoom cl){
		Map<String,Object> map = new HashMap<>();
		map.put("status", dao.addClassRoom(cl));
		return map;
	}
	
	@PutMapping("/updateClass")
	public Map<String, Object> updateClassRoom(@RequestBody ClassRoom cl){
		Map<String,Object> map = new HashMap<>();
		map.put("status", dao.updateClassRoom(cl));
		return map;
	}
}
