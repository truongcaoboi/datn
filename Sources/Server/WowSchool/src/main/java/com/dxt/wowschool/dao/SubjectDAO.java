package com.dxt.wowschool.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.dxt.wowschool.util.Util;
import com.dxt.wowschool.vo.Subject;
import com.dxt.wowschool.vo.SubjectPart;
@Component
public class SubjectDAO {
	public List<Subject> getAllSubject(){
		List<Subject> data = new ArrayList<>();
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			String sql = "select * from subjects";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			rs = state.executeQuery();
			while(rs.next()) {
				Subject sub = new Subject();
				sub.setId(rs.getInt("subject_id"));
				sub.setCode(rs.getString("subject_code"));
				sub.setName(rs.getString("subject_name"));
				sub.setSubjectFinal(rs.getInt("subject_final"));
				data.add(sub);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return data;
	}
	
	public Subject getSubject(int sid) {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		Subject sub = null;
		try {
			String sql = "select * from subjects where subject_id = ?";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setInt(1, sid);
			rs = state.executeQuery();
			while(rs.next()) {
				sub = new Subject();
				sub.setId(rs.getInt("subject_id"));
				sub.setCode(rs.getString("subject_code"));
				sub.setName(rs.getString("subject_name"));
				sub.setSubjectFinal(rs.getInt("subject_final"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return sub;
	}
	
	public List<SubjectPart> getAllSubjectPart(int sub_id){
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		List<SubjectPart> data = new ArrayList<>();
		try {
			String sql = "select * from subject_part where subject_id = ?";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setInt(1, sub_id);
			rs = state.executeQuery();
			while(rs.next()) {
				SubjectPart sub = new SubjectPart();
				sub.setId(rs.getInt("subject_part_id"));
				sub.setCode(rs.getString("subject_part_code"));
				sub.setName(rs.getString("subject_part_name"));
				sub.setSubjectId(rs.getInt("subject_id"));
				data.add(sub);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return data;
	}
	
	public SubjectPart getSubjectPart(int id) {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		SubjectPart sub = null;
		try {
			String sql = "select * from subject_part where subject_part_id = ?";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setInt(1, id);
			rs = state.executeQuery();
			while(rs.next()) {
				sub = new SubjectPart();
				sub.setId(rs.getInt("subject_part_id"));
				sub.setCode(rs.getString("subject_part_code"));
				sub.setName(rs.getString("subject_part_name"));
				sub.setSubjectId(rs.getInt("subject_id"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return sub;
	}
	
	public String getNewSubjectCode() {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		String sql = "select max(subject_id) as id from subjects;";
		String code = null;
		try {
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			rs = state.executeQuery();
			int idMax = 0;
			while(rs.next()) {
				idMax = rs.getInt("id");
			}
			idMax ++;
			code = "SUB"+Util.addPrefix(""+idMax, 4, '0');
		} catch (Exception e) {
			e.printStackTrace();
			code = null;
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return code;
	}
	
	public String getNewSubjectPartCode() {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		String sql = "select max(subject_part_id) as id from subject_part;";
		String code = null;
		try {
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			rs = state.executeQuery();
			int idMax = 0;
			while(rs.next()) {
				idMax = rs.getInt("id");
			}
			idMax ++;
			code = "SUBP"+Util.addPrefix(""+idMax, 4, '0');
		} catch (Exception e) {
			e.printStackTrace();
			code = null;
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return code;
	}
	
	public Subject addSubject(Subject sub) {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		int res = -1;
		try {
			String sql = "insert into subjects(subject_code,subject_name,subject_final) values (?,?,?)";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setString(1, sub.getCode());
			state.setString(2, sub.getName());
			state.setInt(3, sub.getSubjectFinal());
			res = state.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		if(res > 0) {
			sub.setId(res);
			return sub;
		}else {
			return null;
		}
	}
	
	public SubjectPart addSubjectPart(SubjectPart subp) {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		int res = -1;
		try {
			String sql = "insert into subject_part(subject_part_code,subject_part_name, subject_id) values (?,?,?)";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setString(1, subp.getCode());
			state.setString(2, subp.getName());
			state.setInt(3, subp.getSubjectId());
			res = state.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		if(res > 0) {
			subp.setId(res);
			return subp;
		}else {
			return null;
		}
	}
	
	public int updateSubject(Subject sub) {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		int res = -1;
		try {
			String sql = "update subjects set subject_code = ?, subject_name = ?, subject_final = ? where subject_id = ?";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setString(1, sub.getCode());
			state.setString(2, sub.getName());
			state.setInt(3, sub.getSubjectFinal());
			state.setInt(4, sub.getId());
			res = state.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return res;
	}
	
	public int updateSubjectPart(SubjectPart subp) {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		int res = -1;
		try {
			String sql = "update subject_part set subject_part_code = ?, subject_part_name = ? where subject_part_id = ?";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setString(1, subp.getCode());
			state.setString(2, subp.getName());
			state.setInt(3, subp.getId());
			res = state.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return res;
	}
	
	public int addTeacherSubject(int idTeacher, List<Integer> idSubs) {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		int res = -1;
		try {
			String sql = "delete from teacher_subject where teacher_id = ?";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setInt(1, idTeacher);
			state.executeUpdate();
			for(int id : idSubs) {
				sql = "insert into teacher_subject(teacher_id,subject_id) values (?,?)";
				state = conn.prepareStatement(sql);
				state.setInt(1, idTeacher);
				state.setInt(2, id);
				res = state.executeUpdate();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return res;
	}
}
