package com.dxt.wowschool.vo;

import java.util.Calendar;

import com.dxt.wowschool.util.Util;

public class HistoryExam {
	private int id;
	private int examId;
	private int studentId;
	private String examCode;
	private String examName;
	private String startDate;
	private String endDate;
	private double mark;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getExamId() {
		return examId;
	}
	public void setExamId(int examId) {
		this.examId = examId;
	}
	public int getStudentId() {
		return studentId;
	}
	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}
	public String getExamCode() {
		return examCode;
	}
	public void setExamCode(String examCode) {
		this.examCode = examCode;
	}
	public String getExamName() {
		return examName;
	}
	public void setExamName(String examName) {
		this.examName = examName;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(long startDate) {
		Calendar ca = Calendar.getInstance();
		ca.setTimeInMillis(startDate);
		this.startDate = Util.convertDateToString(ca.getTime());
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(long endDate) {
		Calendar ca = Calendar.getInstance();
		ca.setTimeInMillis(endDate);
		this.endDate = Util.convertDateToString(ca.getTime());
	}
	public double getMark() {
		return mark;
	}
	public void setMark(double mark) {
		this.mark = mark;
	}
	
	
}
