package com.dxt.wowschool.vo;

import java.util.Date;

public class ClassExam {
	private int id;
	private int classId;
	private int examId;
	private Date expire;
	private int typeMark;
	private int times;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getClassId() {
		return classId;
	}
	public void setClassId(int classId) {
		this.classId = classId;
	}
	public int getExamId() {
		return examId;
	}
	public void setExamId(int examId) {
		this.examId = examId;
	}
	public Date getExpire() {
		return expire;
	}
	public void setExpire(Date expire) {
		this.expire = expire;
	}
	public int getTypeMark() {
		return typeMark;
	}
	public void setTypeMark(int typeMark) {
		this.typeMark = typeMark;
	}
	public int getTimes() {
		return times;
	}
	public void setTimes(int times) {
		this.times = times;
	}
	
	
}
