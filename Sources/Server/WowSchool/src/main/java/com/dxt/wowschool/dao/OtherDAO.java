package com.dxt.wowschool.dao;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import com.dxt.wowschool.InfoSystem;

@Component
public class OtherDAO {
	public Map<String,Object> changeAvatar(String code, String url){
		Map<String, Object> map = new HashMap<>();
		map.put("status", 0);
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			String sql = "";
			int type = -1;
			if(code.startsWith("AD")) {
				type = InfoSystem.CODE_ADMIN;
				sql = "select admin_avatar as avatar from administrator where admin_code = ?";
			}else if(code.startsWith("TE")) {
				type = InfoSystem.CODE_TEACHER;
				sql = "select teacher_avatar as avatar from teacher where teacher_code = ?";
			}else if(code.startsWith("ST")) {
				type = InfoSystem.CODE_STUDENT;
				sql = "select student_avatar as avatar from student where student_code = ?";
			}
			
			if(!sql.equals("")) {
				conn = MysqlConnector.getMysqlConnector();
				state = conn.prepareStatement(sql);
				state.setString(1, code);
				rs = state.executeQuery();
				String oldAvatar = null;
				while(rs.next()) {
					oldAvatar = rs.getString("avatar");
				}
				if(oldAvatar!=null) {
					if(!oldAvatar.equals("")) {
						try {
							File file = ResourceUtils.getFile("classpath:static/images"+oldAvatar.substring(oldAvatar.lastIndexOf('/')));
							if(file!=null) {
								file.delete();
							}
						} catch (Exception e) {
							// TODO: handle exception
						}
					}
				}
				if(type == InfoSystem.CODE_ADMIN) {
					sql = "update administrator set admin_avatar = ? where admin_code = ?";
				}else if(type == InfoSystem.CODE_TEACHER) {
					sql = "update teacher set teacher_avatar = ? where teacher_code = ?";
				}else {
					sql = "update student set student_avatar = ? where student_code = ?";
				}
				state = conn.prepareStatement(sql);
				state.setString(1, url);
				state.setString(2, code);
				int id = state.executeUpdate();
				if(id > 0) {
					map.put("link", url);
					map.put("status", 1);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return map;
	}

	public Map<String,Object> changeImageQuestion(int idQuestion, String url){
		Map<String, Object> map = new HashMap<>();
		map.put("status", 0);
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			String sql = "select * from question where question_id = ?";
			
			if(!sql.equals("")) {
				conn = MysqlConnector.getMysqlConnector();
				state = conn.prepareStatement(sql);
				state.setInt(1, idQuestion);
				rs = state.executeQuery();
				String oldImage = null;
				while(rs.next()) {
					oldImage = rs.getString("question_image");
				}
				if(oldImage!=null) {
					if(!oldImage.equals("")) {
						try {
							File file = ResourceUtils.getFile("classpath:static/images"+oldImage.substring(oldImage.lastIndexOf('/')));
							if(file!=null) {
								file.delete();
							}
						} catch (Exception e) {
							// TODO: handle exception
						}
					}
				}
				sql = "update question set question_image = ? where question_id = ?";
				state = conn.prepareStatement(sql);
				state.setString(1, url);
				state.setInt(2, idQuestion);
				int id = state.executeUpdate();
				if(id > 0) {
					map.put("link", url);
					map.put("status", 1);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return map;
	}
	public Map<String, Object> deleteImageQuestion(int idQuestion){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("status", -1);
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			String sql = "select question_image from question where question_id = ?";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setInt(1, idQuestion);
			String oldImage = "";
			rs = state.executeQuery();
			while(rs.next()) {
				oldImage = rs.getString("question_image");
			}
			try {
				if(oldImage!=null) {
					if(!oldImage.equals("")) {
						File file = ResourceUtils.getFile("classpath:static/images"+oldImage.substring(oldImage.lastIndexOf('/')));
						if(file!=null) {
							file.delete();
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			state.close();
			sql = "update question set question_image = '' where question_id = ?";
			state = conn.prepareStatement(sql);
			state.setInt(1, idQuestion);
			map.put("status", state.executeUpdate());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MysqlConnector.close(rs , null, state, conn);
		}
		return map;
	}
}
