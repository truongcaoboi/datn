package com.dxt.wowschool.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.dxt.wowschool.util.Util;
import com.dxt.wowschool.vo.ClassRoom;
import com.dxt.wowschool.vo.Subject;
import com.dxt.wowschool.vo.Teacher;

@Component
public class TeacherDAO {
	public List<Teacher> getTeachers() {
		List<Teacher> teachers = new ArrayList<Teacher>();
		Connection conn = null;
		Statement state = null;
		ResultSet rs = null;
		try {
			String sql = "select * from teacher where status = 1";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.createStatement();
			rs = state.executeQuery(sql);
			while (rs.next()) {
				Teacher te = new Teacher();
				te.setId(rs.getInt("teacher_id"));
				te.setCode(rs.getString("teacher_code"));
				te.setName(rs.getString("teacher_name"));
				te.setAvatar(rs.getString("teacher_avatar"));
				te.setGender(rs.getInt("teacher_gender"));
				te.setBirth(Util.changeTimestampSqlToDate(rs.getTimestamp("teacher_birth")));
				te.setAddress(rs.getString("teacher_address"));
				te.setIdentify(rs.getString("teacher_identify"));
				te.setIdentifyAddress(rs.getString("teacher_identify_address"));
				te.setIdentifyDate(Util.changeTimestampSqlToDate(rs.getTimestamp("teacher_identify_date")));
				te.setPhone(rs.getString("teacher_phone"));
				te.setEmail(rs.getString("teacher_email"));
				te.setStatus(rs.getInt("status"));
				te.setTeacherTimeTable(rs.getString("teacher_time_table"));
				teachers.add(te);
			}
		} catch (Exception e) {
			e.printStackTrace();
			teachers = new ArrayList<>();
		} finally {
			MysqlConnector.close(rs, state, null, conn);
		}
		return teachers;
	}

	public List<Teacher> searchTeacher(String code, String name) {
		List<Teacher> teachers = new ArrayList<Teacher>();
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			String sql = "select * from teacher "
					+ "where UPPER(teacher_code) like '*?*' or UPPER(teacher_name) like '*?*'";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setString(1, code.toUpperCase());
			state.setString(2, name.toUpperCase());
			rs = state.executeQuery();
			while (rs.next()) {
				Teacher te = new Teacher();
				te.setId(rs.getInt("teacher_id"));
				te.setCode(rs.getString("teacher_code"));
				te.setName(rs.getString("teacher_name"));
				te.setAvatar(rs.getString("teacher_avatar"));
				te.setGender(rs.getInt("teacher_gender"));
				te.setBirth(Util.changeTimestampSqlToDate(rs.getTimestamp("teacher_birth")));
				te.setAddress(rs.getString("teacher_address"));
				te.setIdentify(rs.getString("teacher_identify"));
				te.setIdentifyAddress(rs.getString("teacher_identify_address"));
				te.setIdentifyDate(Util.changeTimestampSqlToDate(rs.getTimestamp("teacher_identify_date")));
				te.setPhone(rs.getString("teacher_phone"));
				te.setEmail(rs.getString("teacher_email"));
				te.setStatus(rs.getInt("status"));
				te.setTeacherTimeTable(rs.getString("teacher_time_table"));
				teachers.add(te);
			}
		} catch (Exception e) {
			e.printStackTrace();
			teachers = new ArrayList<>();
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return teachers;
	}

	public Teacher getTeacher(int id) {
		Teacher te = null;
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			String sql = "select * from teacher " + "where teacher_id = ?";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setInt(1, id);
			rs = state.executeQuery();
			while (rs.next()) {
				te = new Teacher();
				te.setId(rs.getInt("teacher_id"));
				te.setCode(rs.getString("teacher_code"));
				te.setName(rs.getString("teacher_name"));
				te.setAvatar(rs.getString("teacher_avatar"));
				te.setGender(rs.getInt("teacher_gender"));
				te.setBirth(Util.changeTimestampSqlToDate(rs.getTimestamp("teacher_birth")));
				te.setAddress(rs.getString("teacher_address"));
				te.setIdentify(rs.getString("teacher_identify"));
				te.setIdentifyAddress(rs.getString("teacher_identify_address"));
				te.setIdentifyDate(Util.changeTimestampSqlToDate(rs.getTimestamp("teacher_identify_date")));
				te.setPhone(rs.getString("teacher_phone"));
				te.setEmail(rs.getString("teacher_email"));
				te.setStatus(rs.getInt("status"));
				te.setTeacherTimeTable(rs.getString("teacher_time_table"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			te = null;
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return te;
	}

	public String getNewTeacherCode() {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		String sql = "select max(teacher_id) as teacher_id from teacher;";
		String code = null;
		try {
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			rs = state.executeQuery();
			int idMax = 0;
			while (rs.next()) {
				idMax = rs.getInt("teacher_id");
			}
			idMax++;
			code = "TE" + Util.addPrefix("" + idMax, 4, '0');
		} catch (Exception e) {
			e.printStackTrace();
			code = null;
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return code;
	}

	public Teacher addTeacher(Teacher te) {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		int id = -1;
		String code = getNewTeacherCode();
		if (code == null) {
			return null;
		}
		try {
			String sql = "insert into " + "teacher(teacher_code, teacher_name,"
					+ "teacher_avatar, teacher_gender, teacher_birth, teacher_address,"
					+ "teacher_identify, teacher_identify_address, teacher_identify_date,"
					+ "teacher_phone, teacher_email, status, teacher_time_table) "
					+ "values (?,?,?,?,?,?,?,?,?,?,?,?,?)";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setString(1, code);
			state.setString(2, te.getName());
			state.setString(3, te.getAvatar());
			state.setInt(4, te.getGender());
			state.setTimestamp(5, new Timestamp(te.getBirth().getTime()));
			state.setString(6, te.getAddress());
			state.setString(7, te.getIdentify());
			state.setString(8, te.getIdentifyAddress());
			state.setTimestamp(9, new Timestamp(te.getIdentifyDate().getTime()));
			state.setString(10, te.getPhone());
			state.setString(11, te.getEmail());
			state.setInt(12, 1);
			te.createTimeTable();
			state.setString(13, te.getTeacherTimeTable());
			id = state.executeUpdate();
			te.setId(id);
			te.setCode(code);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		if (id == -1) {
			return null;
		} else {
			return te;
		}
	}

	public int updateTeacher(Teacher te) {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		int result = -1;
		try {
			String sql = "update teacher set " + "teacher_name = ?, teacher_gender = ?,"
					+ "teacher_birth = ?, teacher_address = ?," + "teacher_identify = ?, teacher_identify_address = ?,"
					+ "teacher_identify_date = ?, teacher_phone = ?," + "teacher_email = ?, teacher_time_table=? "
					+ "where teacher_id = ?";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setString(1, te.getName());
			state.setInt(2, te.getGender());
			state.setTimestamp(3, new Timestamp(te.getBirth().getTime()));
			state.setString(4, te.getAddress());
			state.setString(5, te.getIdentify());
			state.setString(6, te.getIdentifyAddress());
			state.setTimestamp(7, new Timestamp(te.getIdentifyDate().getTime()));
			state.setString(8, te.getPhone());
			state.setString(9, te.getEmail());
			state.setString(10, te.getTeacherTimeTable());
			state.setInt(11, te.getId());
			result = state.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			result = -1;
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return result;
	}

	public int deleteTeacher(int id) {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		int result = -1;
		try {
			String sql = "update teacher set " + " status = 0 " + "where teacher_id = ?";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setInt(1, id);
			result = state.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			result = -1;
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return result;
	}

	public boolean changePassword(int idTeacher, String newPassword) {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		boolean result = false;

		String sql = "update teacher set teacher_password = ? where teacher_id = ?";
		try {
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setString(1, newPassword);
			state.setInt(2, idTeacher);
			result = state.execute();
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return result;
	}

	public int verifyAccount(String code, String identify) {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		int result = -1;

		String sql = "select teacher_id from teacher " + "where teacher_code = ? and teacher_identify = ?";
		try {
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setString(1, code);
			state.setString(2, identify);
			rs = state.executeQuery();
			while (rs.next()) {
				result = rs.getInt("teacher_id");
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = -1;
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return result;
	}

	public boolean changeAvatar(int idTeacher, String newAvatar) {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		boolean result = false;

		String sql = "update teacher set teacher_avatar = ? where teacher_id = ?";
		try {
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setString(1, newAvatar);
			state.setInt(2, idTeacher);
			result = state.execute();
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		} finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return result;
	}

	public List<ClassRoom> getClassTeach(int id) {
		List<ClassRoom> data = new ArrayList<>();
		try {
			Teacher teacher = getTeacher(id);
			ClassDAO dao = new ClassDAO();
			int[][] timeTable = teacher.getOrigin();
			List<Integer> ids = new ArrayList<>();
			for (int i = 0; i < 6; i++) {
				for (int j = 0; j < 10; j++) {
					if (timeTable[i][2 * j + 1] != -1) {
						if (!ids.contains(timeTable[i][2 * j + 1])) {
							ids.add(timeTable[i][2 * j + 1]);
							ClassRoom cr = dao.getClassRoom(timeTable[i][2 * j + 1]);
							if (cr != null) {
								data.add(cr);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	public List<Subject> getSubjectWithClassTeach(int id, int classId) {
		List<Subject> data = new ArrayList<>();
		try {
			Teacher teacher = getTeacher(id);
			SubjectDAO dao = new SubjectDAO();
			int[][] timeTable = teacher.getOrigin();
			List<Integer> ids = new ArrayList<>();
			for (int i = 0; i < 6; i++) {
				for (int j = 0; j < 10; j++) {
					if (timeTable[i][2 * j + 1] == classId) {
						if (!ids.contains(timeTable[i][2 * j])) {
							ids.add(timeTable[i][2 * j]);
							Subject sub = dao.getSubject(timeTable[i][2 * j]);
							if (sub != null) {
								data.add(sub);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}
}
