package com.dxt.wowschool.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dxt.wowschool.dao.AdminDAO;
import com.dxt.wowschool.vo.Admin;

@RestController
@RequestMapping("api/admins")
public class AdminController {
	@Autowired
	private AdminDAO adminDao;
	@GetMapping("/allAdmin")
	public List<Admin> getAllAdmin(){
		return adminDao.getAdmins();
	}
	
	@GetMapping("/nextCode")
	public String getNextCode() {
		return adminDao.getNewAdminCode();
	}
	
	@GetMapping("/getbyid/{id}")
	public Admin getAdminById(@PathVariable int id) {
		System.out.println(id);
		Admin admin = adminDao.getAdmin(id);
		return admin;
	}
	
	@PostMapping("/addAdmin")
	public Map<String,Object> addAdmin(@RequestBody Admin ad){
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("status", 0);
		try {
			Admin a = adminDao.addAdmin(ad);
			if(a != null) {
				map.put("status", 1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	@PutMapping("/updateInfo")
	public Map<String,Object> updateInfo(@RequestBody Admin ad){
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("status", 0);
		try {
			map.put("status", adminDao.updateAdmin(ad));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	@DeleteMapping("/deleteAdmin/{id}")
	public Map<String,Object> deleteAdmin(@PathVariable int id){
		Map<String,Object> map = new HashMap<>();
		map.put("status", 0);
		map.put("status", adminDao.deleteAdmin(id));
		return map;
	}
	
	@GetMapping("/seach")
	public List<Admin> search(@RequestParam String content){
		return adminDao.searchAdmin(content, content);
	}
}
