package com.dxt.wowschool.vo;

public class Question {
	private int id;
	private String code;
	private String content;
	private String image;
	private String answers;
	private int subjectId;
	private String subjectName;
	private int subjectPartId;
	private String subjectPartName;
	private int level;
	private String levelText;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getAnswers() {
		return answers;
	}
	public void setAnswers(String answers) {
		this.answers = answers;
	}
	public int getSubjectPartId() {
		return subjectPartId;
	}
	public void setSubjectPartId(int subjectPartId) {
		this.subjectPartId = subjectPartId;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public int getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(int subjectId) {
		this.subjectId = subjectId;
	}
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	public String getSubjectPartName() {
		return subjectPartName;
	}
	public void setSubjectPartName(String subjectPartName) {
		this.subjectPartName = subjectPartName;
	}
	public String getLevelText() {
		return levelText;
	}
	public void setLevelText(String levelText) {
		this.levelText = levelText;
	}
	
	
}
