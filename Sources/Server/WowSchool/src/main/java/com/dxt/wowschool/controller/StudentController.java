package com.dxt.wowschool.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dxt.wowschool.dao.StudentDAO;
import com.dxt.wowschool.packet.PacketClassInfo;
import com.dxt.wowschool.vo.Student;

@RestController
@RequestMapping("/api/students")
public class StudentController {
	@Autowired
	private StudentDAO studentDao;
	@GetMapping("/allStudent")
	public List<Student> getAllStudent(){
		return studentDao.getStudents();
	}
	
	@GetMapping("/allStudent/{classId}")
	public List<Student> getAllStudentByClassId(@PathVariable int classId){
		return studentDao.getStudentsByClassId(classId);
	}
	
	@GetMapping("/nextCode")
	public String getNextCode() {
		return studentDao.getNewStudentCode();
	}
	
	@GetMapping("/getbyid/{id}")
	public Student getStudentById(@PathVariable int id) {
//		System.out.println(id);
		Student admin = studentDao.getStudent(id);
		return admin;
	}
	
	@PostMapping("/addStudent")
	public Map<String,Object> addStudent(@RequestBody Student ad){
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("status", 0);
		try {
			Student a = studentDao.addStudent(ad);
			if(a != null) {
				map.put("status", 1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	@PutMapping("/updateInfo")
	public Map<String,Object> updateInfo(@RequestBody Student ad){
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("status", 0);
		try {
			map.put("status", studentDao.updateStudent(ad));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	@DeleteMapping("/deleteStudent/{id}")
	public Map<String,Object> deleteStudent(@PathVariable int id){
		Map<String,Object> map = new HashMap<>();
		map.put("status", 0);
		map.put("status", studentDao.deleteStudent(id));
		return map;
	}
	
	@GetMapping("/seach")
	public List<Student> search(@RequestParam String content){
		return studentDao.searchStudent(content, content);
	}
	
	@GetMapping("/getTableMark/{id}/{semId}")
	public double[][] getTableMark(@PathVariable int id, @PathVariable int semId){
		double [][] re = null;
		Student st = studentDao.getStudent(id);
		re = studentDao.getTableMark(st, semId);
		return re;
	}
	
	@PutMapping("/updateTableMark")
	public Map<String,Object> updateTableMark(@RequestBody Map<String,Object> body){
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("status", 0);
		try {
			int id = (int) body.get("id");
			String tablemark = (String) body.get("tableMark");
			map.put("status", studentDao.updateTableMark(id, tablemark));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	@GetMapping("/getTimeTable/{id}")
	public String getTimeTable(@PathVariable int id) {
		return studentDao.getTimeTable(id);
	}
	
	@GetMapping("/getClassmate/{id}")
	public PacketClassInfo getClassmate(@PathVariable int id){
		return studentDao.getClassMate(id);
	}
}
