package com.dxt.wowschool.vo;

public class Subject {
	private int id;
	private String code;
	private String name;
	private int subjectFinal;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSubjectFinal() {
		return subjectFinal;
	}
	public void setSubjectFinal(int subjectFinal) {
		this.subjectFinal = subjectFinal;
	}
}
