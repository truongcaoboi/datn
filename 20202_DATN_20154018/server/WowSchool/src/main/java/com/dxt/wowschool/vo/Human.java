package com.dxt.wowschool.vo;

import java.util.Calendar;
import java.util.Date;

public class Human {
	protected int id;
	protected String code;
	protected String name;
	protected String password;
	protected String avatar;
	protected int gender;
	protected long birth;
	protected String address;
	protected String identify;
	protected String identifyAddress;
	protected long identifyDate;
	protected String phone;
	protected String email;
	protected int status;//0 laf xoa r - 1 la dang hoat dong
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public int getGender() {
		return gender;
	}
	public void setGender(int gender) {
		this.gender = gender;
	}
	public Date getBirth() {
		Calendar ca = Calendar.getInstance();
		ca.setTimeInMillis(birth);
		return ca.getTime();
	}
	public void setBirth(Date birth) {
		this.birth = birth.getTime();
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getIdentify() {
		return identify;
	}
	public void setIdentify(String identify) {
		this.identify = identify;
	}
	public String getIdentifyAddress() {
		return identifyAddress;
	}
	public void setIdentifyAddress(String identifyAddress) {
		this.identifyAddress = identifyAddress;
	}
	public Date getIdentifyDate() {
		Calendar ca = Calendar.getInstance();
		ca.setTimeInMillis(identifyDate);
		return ca.getTime();
	}
	public void setIdentifyDate(Date identifyDate) {
		this.identifyDate = identifyDate.getTime();
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	
}
