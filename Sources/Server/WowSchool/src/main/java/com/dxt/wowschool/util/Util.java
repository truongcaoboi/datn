package com.dxt.wowschool.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.google.gson.Gson;

public class Util {
	public static final Gson gson = new Gson();
	
	public static Date changeTimestampSqlToDate(Timestamp ts) {
		Calendar ca = Calendar.getInstance();
		if(ts != null) {
			ca.setTimeInMillis(ts.getTime());
		}
		return ca.getTime();
	}
	
	public static String addPrefix(String str, int maxLenght, char character) {
		int n = maxLenght - str.length();
		for(int i =0;i<n;i++) {
			str = character + str;
		}
		return str;
	}
	
	public static String addSubfix(String str, int maxLenght, char character) {
		int n = maxLenght - str.length();
		for(int i =0;i<n;i++) {
			str += character;
		}
		return str;
	}
	
	public static String convertDateToString(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		return format.format(date);
	}
}
