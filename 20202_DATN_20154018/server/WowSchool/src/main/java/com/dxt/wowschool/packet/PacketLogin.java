package com.dxt.wowschool.packet;


public class PacketLogin {
	private String username = "";
	private int idUser = -1;
	private String fullNameUser = "";
	private int usertype = -1;
	private int status = 0;
	private String message = "";
	private String token = "";
	private String avatar = "";
	private int gender = -1;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public String getFullNameUser() {
		return fullNameUser;
	}
	public void setFullNameUser(String fullNameUser) {
		this.fullNameUser = fullNameUser;
	}
	public int getUsertype() {
		return usertype;
	}
	public void setUsertype(int usertype) {
		this.usertype = usertype;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public int getGender() {
		return gender;
	}
	public void setGender(int gender) {
		this.gender = gender;
	}
	
	
}
