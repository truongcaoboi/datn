package com.dxt.wowschool.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.dxt.wowschool.util.Util;
import com.dxt.wowschool.vo.ClassRoom;
import com.dxt.wowschool.vo.Subject;

@Component
public class ClassDAO {
	public List<ClassRoom> getAllClassRoom(){
		List<ClassRoom> data = new ArrayList<>();
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			String sql = "select cl.* , (select count(*) from student where student.class_id = cl.class_id) as ss, teacher.teacher_name as teacher_name "
					+ " from class as cl, teacher where cl.class_teacher = teacher.teacher_id";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			rs = state.executeQuery();
			while(rs.next()) {
				ClassRoom cl = new ClassRoom();
				cl.setId(rs.getInt("class_id"));
				cl.setCode(rs.getString("class_code"));
				cl.setName(rs.getString("class_name"));
				cl.setTeacherMainId(rs.getInt("class_teacher"));
				cl.setTeacherMainName(rs.getString("teacher_name"));
				cl.setStatus(rs.getInt("class_status"));
				cl.setGrade(rs.getInt("grade"));
				cl.setTimeTable(rs.getString("class_timetable"));
				cl.setSs(rs.getInt("ss"));
				data.add(cl);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return data;
	}
	
	public List<ClassRoom> getClassBySemester(int sem_id){
		List<ClassRoom> data = new ArrayList<>();
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			String sql = "select cl.*, (select count(*) from student where student.class_id = cl.class_id) as ss, teacher.teacher_name as teacher_name "
					+ " from class as cl, teacher where cl.class_id in (select distinct class_id from student_semester where sem_id = ?) and cl.class_teacher = teacher.teacher_id";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setInt(1, sem_id);
			rs = state.executeQuery();
			while(rs.next()) {
				ClassRoom cl = new ClassRoom();
				cl.setId(rs.getInt("class_id"));
				cl.setCode(rs.getString("class_code"));
				cl.setName(rs.getString("class_name"));
				cl.setTeacherMainId(rs.getInt("class_teacher"));
				cl.setTeacherMainName(rs.getString("teacher_name"));
				cl.setStatus(rs.getInt("class_status"));
				cl.setGrade(rs.getInt("grade"));
				cl.setTimeTable(rs.getString("class_timetable"));
				cl.setSs(rs.getInt("ss"));
				data.add(cl);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return data;
	}
	
	
	public List<ClassRoom> getClassInSemester(){
		List<ClassRoom> data = new ArrayList<>();
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			String sql = "select cl.*, (select count(*) from student where student.class_id = cl.class_id) as ss, teacher.teacher_name as teacher_name "
					+ " from class as cl, teacher where cl.class_id in (select distinct class_id from student_semester where sem_id = (select max(sem_id) from semester)) and cl.class_teacher = teacher.teacher_id";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			rs = state.executeQuery();
			while(rs.next()) {
				ClassRoom cl = new ClassRoom();
				cl.setId(rs.getInt("class_id"));
				cl.setCode(rs.getString("class_code"));
				cl.setName(rs.getString("class_name"));
				cl.setTeacherMainId(rs.getInt("class_teacher"));
				cl.setTeacherMainName(rs.getString("teacher_name"));
				cl.setStatus(rs.getInt("class_status"));
				cl.setGrade(rs.getInt("grade"));
				cl.setTimeTable(rs.getString("class_timetable"));
				cl.setSs(rs.getInt("ss"));
				data.add(cl);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return data;
	}
	
	public List<ClassRoom> getClassNotInSemester(int sem_id){
		List<ClassRoom> data = new ArrayList<>();
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			String sql = "select cl.*, (select count(*) from student where student.class_id = cl.class_id) as ss, teacher.teacher_name as teacher_name "
					+ " from class as cl, teacher where cl.class_id not in (select distinct class_id from student_semester where sem_id = ?) and cl.class_teacher = teacher.teacher_id";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setInt(1, sem_id);
			rs = state.executeQuery();
			while(rs.next()) {
				ClassRoom cl = new ClassRoom();
				cl.setId(rs.getInt("class_id"));
				cl.setCode(rs.getString("class_code"));
				cl.setName(rs.getString("class_name"));
				cl.setTeacherMainId(rs.getInt("class_teacher"));
				cl.setTeacherMainName(rs.getString("teacher_name"));
				cl.setStatus(rs.getInt("class_status"));
				cl.setGrade(rs.getInt("grade"));
				cl.setTimeTable(rs.getString("class_timetable"));
				cl.setSs(rs.getInt("ss"));
				data.add(cl);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return data;
	}
	
	public ClassRoom getClassRoom(int idClass) {
		System.out.println(idClass);
		ClassRoom cl = null;
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			String sql = "select cl.*, (select count(*) from student where student.class_id = cl.class_id) as ss, teacher.teacher_name as teacher_name "
					+ " from class as cl, teacher where cl.class_id = ? and cl.class_teacher = teacher.teacher_id";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setInt(1, idClass);
			rs = state.executeQuery();
			while(rs.next()) {
				cl = new ClassRoom();
				cl.setId(rs.getInt("class_id"));
				cl.setCode(rs.getString("class_code"));
				cl.setName(rs.getString("class_name"));
				cl.setTeacherMainId(rs.getInt("class_teacher"));
				cl.setTeacherMainName(rs.getString("teacher_name"));
				cl.setStatus(rs.getInt("class_status"));
				cl.setGrade(rs.getInt("grade"));
				cl.setTimeTable(rs.getString("class_timetable"));
				cl.setSs(rs.getInt("ss"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return cl;
	}
	
	public String getNewClassCode() {
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		String sql = "select max(class_id) as id from class;";
		String code = null;
		try {
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			rs = state.executeQuery();
			int idMax = 0;
			while(rs.next()) {
				idMax = rs.getInt("id");
			}
			idMax ++;
			code = "CLS"+Util.addPrefix(""+idMax, 4, '0');
		} catch (Exception e) {
			e.printStackTrace();
			code = null;
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return code;
	}
	
	public int addClassRoom(ClassRoom cl) {
		int res = -1;
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			String sql = "insert into class(class_code,class_name, class_teacher,class_status,grade,class_timetable) values(?,?,?,?,?,?)";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setString(1, cl.getCode());
			state.setString(2, cl.getName());
			state.setInt(3, cl.getTeacherMainId());
			state.setInt(4, cl.getStatus());
			state.setInt(5, cl.getGrade());
			cl.createTimeTable();
			state.setString(6, cl.getTimeTable());
			res = state.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return res;
	}
	
	public int updateClassRoom(ClassRoom cl) {
		int res = -1;
		Connection conn = null;
		PreparedStatement state = null;
		ResultSet rs = null;
		try {
			String sql = "update class set "
					+ "class_code = ?, class_name = ?, class_teacher = ?, class_status = ?, grade = ?, class_timetable = ? "
					+ "where class_id = ?";
			conn = MysqlConnector.getMysqlConnector();
			state = conn.prepareStatement(sql);
			state.setString(1, cl.getCode());
			state.setString(2, cl.getName());
			state.setInt(3, cl.getTeacherMainId());
			state.setInt(4, cl.getStatus());
			state.setInt(5, cl.getGrade());
			state.setString(6, cl.getTimeTable());
			state.setInt(7, cl.getId());
			res = state.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			MysqlConnector.close(rs, null, state, conn);
		}
		return res;
	}
	
	
}
