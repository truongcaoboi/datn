package com.dxt.wowschool.vo;

public class TimeTableStudent {
	private int[][] tableTable = new int[][] {
		{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},
		{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},
		{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},
		{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},
		{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},
		{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},
		{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},
	};

	public int[][] getTableTable() {
		return tableTable;
	}

	public void setTableTable(int[][] tableTable) {
		this.tableTable = tableTable;
	}
	
	
}
