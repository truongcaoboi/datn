package com.dxt.wowschool;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.CacheControl;
import org.springframework.util.ResourceUtils;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class WowSchoolApplication {

	public static void main(String[] args) {
		SpringApplication.run(WowSchoolApplication.class, args);
	}

	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**").allowedOrigins("http://localhost:8080");
				registry.addMapping("/**").allowedMethods("GET", "POST", "DELETE", "PUT");
			}
			
			@Override
			public void addResourceHandlers(ResourceHandlerRegistry registry) {

			    // Register resource handler for images
			    registry.addResourceHandler("/resource/images/**").addResourceLocations("classpath:static/images/")
			            .setCacheControl(CacheControl.maxAge(2, TimeUnit.HOURS).cachePublic());
			}
		};
	}
}
